import fetch from 'node-fetch'
import getConfig from 'next/config'
const { publicRuntimeConfig } = getConfig()
const { NODE_ENV } = publicRuntimeConfig

const testKeys = {
  grant_type: 'authorization_code',
  clientID: 'NSMRiF71NE4Ez6n0Ky2D',
  clientSecret: 'lX9A_Co01v',
  redirectUri: decodeURIComponent('http://localhost:3000/callback/naver'),
}

const productionKeys = {
  grant_type: 'authorization_code',
  clientID: 'd1t8_wtHNSPy9FkxHTy9',
  clientSecret: 'KnJmlqrobL',
  redirectUri: decodeURIComponent('https://bigsearch.kr/callback/naver'),
}

export const naver = NODE_ENV === 'development' ? testKeys : productionKeys

const profileUrl = 'https://openapi.naver.com/v1/nid/me'

// 접근 토큰 발급 요청
const generateTokenUrl = (info) => {
  const { code, clientID, clientSecret, redirectUri, grant_type } = info
  return `https://nid.naver.com/oauth2.0/token?grant_type=${grant_type}&client_id=${clientID}&client_secret=${clientSecret}&redirect_uri=${redirectUri}&code=${code}`
}

// 접근 토큰 삭제 요청
export const getDeleteTokenUrl = (accessToken) => {
  return `https://nid.naver.com/oauth2.0/token?grant_type=delete&client_id=${naver.clientID}&client_secret=${naver.clientSecret}&access_token=${accessToken}&service_provider=NAVER`
}


export async function getNaverAccessToken({
  code,
  clientID,
  clientSecret,
  redirectUri,
  grant_type,
}) {
  const UrlToGetAccessToken = generateTokenUrl({
    code,
    clientID,
    clientSecret,
    redirectUri,
    grant_type,
  })
  const response = await fetch(UrlToGetAccessToken, {
    method: 'GET',
    headers: {
      'X-Naver-Client-Id': clientID,
      'X-Naver-Client-Secret': clientSecret,
    },
  }).then(async (res) => {
    const json = await res.json()
    return json
  })

  return response.access_token
}

export async function getNaverProfile(token) {
  console.log('get naver profile...')

  const res = await fetch(profileUrl, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json())

  if (res.message === 'success') {
    console.log(res.response)
    const credentials = res.response
    return {
      id: credentials.id,
      name: credentials.name,
      email: credentials.email,
    }
  } else {
    return null
  }
}

export function getNaverRedirect({ clientID, redirectUri }) {
  const url = `https://nid.naver.com/oauth2.0/authorize?response_type=code&client_id=${clientID}&redirect_uri=${redirectUri}`
  return url
}
