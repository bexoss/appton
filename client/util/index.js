
export const stringToUrl = (string) => {
  if(!string) return
  if (string.includes('/')) {
    return string.replace(/\//g, '%2F')
  }
  return string
}

export const urlToString = (url) => {
  if(!url) return
  if (url.includes('%2F')) {
    return url.replace('%2F', '/')
  }

  return url
}