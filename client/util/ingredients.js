import { Button, Icon } from 'semantic-ui-react'

export const getSkinDeepDescription = (score) => {
    if (score < 3) {
        return (
            <span
                style={{
                    color: 'blue',
                    fontWeight: '600',
                    whiteSpace: 'nowrap',
                }}
            >
                [안전]
            </span>
        )
    }
    if (score >= 3 && score <= 6) {
        return (
            <span
                style={{
                    color: 'orange',
                    fontWeight: '600',
                    whiteSpace: 'nowrap',
                }}
            >
                [주의]
            </span>
        )
    }
    if (score > 6) {
        return (
            <span
                style={{
                    color: '#E63C2F',
                    fontWeight: '600',
                    whiteSpace: 'nowrap',
                }}
            >
                [위험]
            </span>
        )
    }
}

export const getSkinDeepLevel = (score, score_min) => {
    if (score !== score_min) {
        return <span>등급. 사용 방법에 따름</span>
    } else {
        return <span>등급</span>
    }
}

export const getSkinDeepDataLevelDescription = (level) => {
    let desc = null
    if (level === 'None') {
        desc = (
            <p>
                <strong style={{ color: 'red' }}>[{level}] </strong>
                평가 데이터가 <strong>확보되지 않음.</strong>
            </p>
        )
    }
    if (level === 'Limited') {
        desc = (
            <p>
                <strong style={{ color: 'red' }}>[{level}] </strong>
                평가 데이터가 <strong>충분하지 않음.</strong>
            </p>
        )
    }
    if (level === 'Fair') {
        desc = (
            <p>
                <strong style={{ color: 'green' }}>[{level}] </strong>
                평가 데이터가 <strong>적정한 편임.</strong>
            </p>
        )
    }
    if (level === 'Good') {
        desc = (
            <p>
                <strong style={{ color: 'green' }}>[{level}] </strong>
                평가 데이터가 <strong>충분하게 확보됨.</strong>
            </p>
        )
    }
    if (level === 'Robust') {
        desc = (
            <p>
                <strong style={{ color: 'green' }}>[{level}] </strong>
                평가 데이터가 <strong>신뢰할 수 있음.</strong>
            </p>
        )
    }
    return desc
}

export const getSkinDeepDataLevelDescriptionText = (level) => {
    if (level === 'None') return '평가 데이터가 확보되지 않은(None) 편이다.'
    if (level === 'Limited')
        return '평가 데이터가 충분하지 않은(Limited) 편이다.'
    if (level === 'Fair') return '평가 데이터가 적정한(Fair) 편이다.'
    if (level === 'Good') return '평가 데이터가 충분하게(Good) 확보되었다.'
    if (level === 'Robust') return '평가 데이터를 신뢰할 수 있다.(Robust)'
    return ''
}

export const Limitation = ({ arr, country, handleClick }) => {
    const limit = arr.filter((el) => {
        return el.country === country
    })
    if (limit.length > 0) {
        const level = limit[0].level
        const nb = limit[0].nb

        if (level === '금지') {
            return (
                <span style={{ color: '#E63C2F', fontWeight: 600 }}>금지</span>
            )
        } else if (level === '한도') {
            return (
                <span style={{ color: 'darkred', fontWeight: 600 }}>
                    한도
                    {nb && (
                        <Icon
                            name="zoom-in"
                            className="hover-trans"
                            style={{ cursor: 'pointer' }}
                            onClick={handleClick}
                        />
                    )}
                </span>
            )
        }
    } else {
        return (
            <span>
                <Icon style={{ color: '#05a000' }} name="check" />
            </span>
        )
    }
}
