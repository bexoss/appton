import gql from 'graphql-tag'

export const GET_SINGLE_COSMETICS = gql`
  query GetSingleCosmetics($productName: String!) {
    getSingleCosmetics(productName: $productName) {
      productCode
      productName
      description
      brand
      madeIn
      madeBy
      seller
      image
      price
      ingredients
      url
      dueDate
    }
  }
`

export const CREATE_PULL_REQUEST = gql`
  mutation CreatePullRequest($targetId: ID!, $targetName: String, $targetType: String, $diff: [DiffInput], $comment: String!) {
    createPullRequest(targetId: $targetId, targetName: $targetName, targetType: $targetType, diff: $diff, comment: $comment) {
      id
    }
  }
`

export const CREATE_PULL_COMMENT = gql`
  mutation CreatePullComment($id: ID!, $comment: String!) {
    createPullComment(id: $id, comment: $comment) {
      id
      comments {
        _id
        comment
        user {
          email
          name
        }
        createdAt
      }
    }
  }
`

export const UPDATE_PULL_REQUEST = gql`
  mutation UpdatePullRequest($id: ID!, $targetId: ID!, $status: String, $accept: [AcceptPullRequestInput], $comment: String) {
    updatePullRequest(id: $id, targetId: $targetId, status: $status, accept: $accept, comment: $comment) {
      id
      comments {
        _id
        comment
        user {
          email
          name
        }
      }
    }
  }
`

export const REMOVE_PULL_COMMENT = gql`
  mutation RemovePullComment($id: ID!, $commentId: ID!) {
    removePullComment(id: $id, commentId: $commentId) {
      id
      comments {
        _id
        comment
        user {
          email
          name
        }
        createdAt
      }
    }
  }
`

export const GET_SINGLE_PULL_REQUEST = gql`
  query GetSinglePullRequest($id: String!) {
    getSinglePullRequest(id: $id) {
      id
      status
      targetId
      targetName
      user {
        email
        name
      }
      comments {
        _id
        comment
        createdAt
        user {
          email
          name
        }
      }
      diff {
        key
        before
        after
      }
      logs {
        status
        update {
          key
          after
        }
        createdAt
      }
      createdAt
      updatedAt
      acceptedAt
      deniedAt
    }
  }
`

export const GET_PULL_REQUESTS = gql`
  query GetPullRequests {
    getPullRequests {
      id
      status
      targetId
      targetName
      user {
        email
        name
      }
      comments {
        _id
        comment
        createdAt
        user {
          email
          name
        }
      }
      createdAt
      updatedAt
      acceptedAt
      deniedAt
    }
  }
`

export const CREATE_ORDER = gql`
  mutation CreateOrder($input: OrderInput) {
    createOrder(input: $input) {
      id
    }
  }
`

export const UPDATE_ORDER = gql`
  mutation UpdateOrder($id: String!, $key: String, $value: String) {
    updateOrder(id: $id, key: $key, value: $value) {
      id
      buyer
      buyerName
      buyerContact
      buyerEmail
      note
      product
      weight
      isMoq
    }
  }
`

export const CANCEL_ORDER = gql`
  mutation CancelOrder($ids: String!) {
    cancelOrder(ids: $ids) {
      id
    }
  }
`

export const GET_SINGLE_INGREDIENT = gql`
  query GetSingleIngredient($nameKor: String!, $hash: String) {
    getSingleIngredient(nameKor: $nameKor, hash: $hash) {
      ingredient {
        id
        nameKor
        nameEng
        nameEngPrevious
        nameKorPrevious
        description
        casNo
        ecNo
        rationalFormula
        functions
        image
        skinDeepName
        skinDeepScore
        skinDeepDataLevel
        skinDeepImageSrc
        skinDeepUrl
        skinDeepSimilarity
        lastCheckedAt
        updatedAt
        createdAt
        structureImage
        skinDeepDescription
        skinDeepFunctions
        skinDeepSynonyms
        euCosingRefNo
        euFunction
        euINCIName
        euChemNameAndDescription
        euRestriction
        euUpdatedAt
        euINNName
        euEcNo
        euCasNo
        INCI
        nameCn
        nameJp
        cnRefNo
        descJp
        jpRefNo
        limitedCountries {
          country
          level
          nb
          materialsKo
          materialsEn
        }
      }
      matchingProducts {
        id
        name
        type
        functions
        manufacture
        note
        INCIs
      }
    }
  }
`

export const GET_INGREDIENTS_LIST = gql`
  query GetIngredientsList($ingredients: String!) {
    getIngredientsList(ingredients: $ingredients) {
      id
      nameKor
      nameEng
      nameEngPrevious
      nameKorPrevious
      description
      casNo
      ecNo
      rationalFormula
      functions
      image
      skinDeepName
      skinDeepScore
      skinDeepDataLevel
      skinDeepImageSrc
      skinDeepUrl
      skinDeepSimilarity
      lastCheckedAt
      updatedAt
      createdAt
    }
  }
`

export const GET_ALL_COSMETICS = gql`
  query GetAllCosmetics {
    getAllCosmetics {
      productCode
      productName
      description
      brand
      madeIn
      madeBy
      seller
      image
      price
      ingredients
    }
  }
`

export const GET_COSMETICS = gql`
  query GetCosmetics($productName: String!) {
    getCosmetics(productName: $productName) {
      title
      description
      price
      image
      type
    }
  }
`

export const GET_BRAND_ITEMS = gql`
  query GetBrandItems($brand: String!) {
    getBrandItems(brand: $brand) {
      productCode
      productName
      description
      price
      image
      brand
      seller
      madeBy
    }
  }
`

export const SIGN_UP = gql`
  mutation SignUp($email: String!, $name: String!, $password: String!, $company: String) {
    signUp(email: $email, name: $name, password: $password, company: $company) {
      id
      email
      name
      company
      mobile
      level
      token
    }
  }
`

export const SIGN_IN = gql`
  mutation SignIn($email: String!, $password: String!) {
    signIn(email: $email, password: $password) {
      id
      email
      name
      mobile
      company
      level
      token
    }
  }
`

export const GET_NEW_TOKEN = gql`
  query GetNewToken($email: String!) {
    getNewToken(email: $email)
  }
`

export const RESET_PASSWORD = gql`
  mutation ResetPassword($email: String!) {
    resetPassword(email: $email)
  }
`

export const RESET_PASSWORD_CONFIRM = gql`
  mutation ResetPasswordConfirm($token: String!, $password: String!) {
    resetPasswordConfirm(token: $token, password: $password)
  }
`

export const GET_POSTS = gql`
  query GetPosts($mid: String!, $perPage: Int, $page: Int, $search: String) {
    getPosts(mid: $mid, page: $page, search: $search, perPage: $perPage) {
      id
      mid
      title
      content
      isNotice
      isSecret
      user {
        name
        email
      }
      createdAt
      updatedAt
      viewCount
      comments {
        _id
        user {
          email
          name
        }
        content
        createdAt
      }
    }
  }
`

export const GET_SINGLE_POST = gql`
  query GetSinglePost($id: String!, $dayFirstUnix: String) {
    getSinglePost(id: $id, dayFirstUnix: $dayFirstUnix) {
      id
      mid
      title
      content
      user {
        name
        email
      }
      createdAt
      updatedAt
      viewCount
      comments {
        _id
        user {
          email
          name
        }
        content
        createdAt
      }
    }
  }
`

export const ADD_POST = gql`
  mutation AddPost($title: String!, $content: String!, $mid: String!) {
    addPost(postInput: { title: $title, content: $content }, mid: $mid) {
      id
      title
      content
      user {
        name
        email
      }
      createdAt
      status
      isNotice
      isSecret
    }
  }
`

export const UPDATE_POST = gql`
  mutation UpdatePost($id: ID!, $title: String!, $content: String!) {
    updatePost(id: $id, postInput: { title: $title, content: $content }) {
      id
      title
      content
      user {
        name
        email
      }
      createdAt
    }
  }
`

export const REMOVE_POST = gql`
  mutation RemovePost($id: ID!) {
    removePost(id: $id)
  }
`

export const ADD_COMMENT = gql`
  mutation AddComment($postId: ID!, $content: String!) {
    addComment(content: $content, postId: $postId) {
      id
      comments {
        _id
        createdAt
        updatedAt
        content
        user {
          email
          name
        }
      }
    }
  }
`

export const REMOVE_COMMENT = gql`
  mutation RemoveComment($postId: ID!, $commentId: ID!) {
    removeComment(commentId: $commentId, postId: $postId) {
      id
      comments {
        _id
        createdAt
        updatedAt
        content
        user {
          email
          name
        }
      }
    }
  }
`
export const UPDATE_COMMENT = gql`
  mutation UpdateComment($postId: ID!, $commentId: ID!, $content: String!) {
    updateComment(commentInput: { content: $content }, postId: $postId, commentId: $commentId) {
      id
      comments {
        _id
        createdAt
        updatedAt
        content
        user {
          email
          name
        }
      }
    }
  }
`

export const UPDATE_USER = gql`
  mutation UpdateUser($email: String!, $name: String!, $company: String, $mobile: String) {
    updateUser(editUserInput: { name: $name, mobile: $mobile, company: $company }, email: $email)
  }
`

export const REMOVE_USER = gql`
  mutation RemoveUser($email: String!) {
    removeUser(email: $email)
  }
`

export const GET_ORDERS = gql`
  query GetOrders {
    getOrders {
      id
      pid
      buyer
      buyerName
      buyerContact
      buyerEmail
      note
      product
      weight
      isMoq
      isSample
      status
      createdAt
      manufacture
      dealer
      INCIs
      INCIsKor
    }
  }
`

export const CREATE_SUBSCRIPTION = gql`
  mutation CreateSubscription($email: String!) {
    createSubscription(email: $email)
  }
`

export const TOGGLE_SUBSCRIPTION = gql`
  mutation toggleSubscription($email: String!, $alive: Boolean!) {
    toggleSubscription(email: $email, alive: $alive) {
      id
      createdAt
      name
      mobile
      email
      nextPayment
      nextPaymentAmount
      payments {
        createdAt
        amount
      }
      alive
    }
  }
`
