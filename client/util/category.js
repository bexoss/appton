export const businessCategoryOptions = [
  { value: 'electric', text: '전기/전자' },
  { value: 'bio', text: '바이오/제약' },
  { value: 'trade', text: '소비재/수출' },
  { value: 'domestic', text: '소비재/내수' },
  { value: 'distribution', text: '유통' },
  { value: 'mobile', text: '모바일/게임' },
  { value: 'it', text: 'IT' },
  { value: 'bank', text: '금융/증권' },
  { value: 'ship', text: '자동차/선박' },
  { value: 'steel', text: '철강' },
  { value: 'fourth', text: '4차산업' },
  { value: 'casino', text: '카지노/유흥' },
]

export const getKorCategory = (value) => {
  return businessCategoryOptions.find(x => x.value === value).text
}

