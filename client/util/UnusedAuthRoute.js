import React, { useContext } from 'react';
import { useRouter } from 'next/router'
import { AuthContext } from '../context/auth';
import Link from 'next/link'


function AuthRoute({ component: Component, ...rest }) {
  const router = useRouter()
  const { user } = useContext(AuthContext);

  console.log('Auth route...')
  console.log(user)
  
  if (user) {
    if (router.pathname === '/login' || router.pathname === '/register') {
      router.push('/')
    }
  }

  return (
    <Link {...rest} />
  )


}

export default AuthRoute;