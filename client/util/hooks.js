import { useState, useEffect, useContext, useCallback } from 'react'
import gql from 'graphql-tag'
import { useLazyQuery, useQuery, useMutation } from '@apollo/react-hooks'
import { GET_COUNT } from '../util/gql-admin'



export const useUserContext = (userContext) => {
  const [user, setUser] = useState(null)
  const context = useContext(userContext)
  
  useEffect(() => {
    setUser(context.user)
  }, [])
  
  return user
}

export const useForm = (callback, initialState = {}) => {
  const [values, setValues] = useState(initialState)

  const onChange = (event) => {
    setValues({ ...values, [event.target.name]: event.target.value })
  }

  const onSubmit = (event) => {
    event.preventDefault()
    callback()
  }

  return {
    onChange,
    onSubmit,
    values,
  }
}

export const useCount = (q) => {
  let query = {}
  if (q.collection) query['collection'] = q.collection
  if (q.search) query['search'] = q.search
  if (q.filter) query['filter'] = q.filter
  if (q.mid) query['mid'] = q.mid

  const [loadCount, { called, loading, data }] = useLazyQuery(GET_COUNT, {
    variables: query,
  })
  return {
    loadCount,
    loading,
    data,
  }
}

export const useWindowDimensions = () => {
  const hasWindow = typeof window !== 'undefined'

  function getWindowDimensions() {
    const width = hasWindow ? window.innerWidth : null
    const height = hasWindow ? window.innerHeight : null
    return {
      width,
      height,
    }
  }

  const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions())

  useEffect(() => {
    if (hasWindow) {
      function handleResize() {
        setWindowDimensions(getWindowDimensions())
      }

      window.addEventListener('resize', handleResize)
      return () => window.removeEventListener('resize', handleResize)
    }
  }, [hasWindow])

  return windowDimensions
}
