const getErrorMessage = (error) => {
  console.log('get ..err')
  if (error.graphQLErrors[0]) {
    return { message: error.graphQLErrors[0].message }
  }

  if (error.networkError && error.networkError.result) {
    return { message: error.networkError.result.errors[0].message }
  }

  return { message: error.message }
}


export default getErrorMessage