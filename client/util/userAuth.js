import Cookies from 'universal-cookie'
import jwtDecode from 'jwt-decode'
import moment from 'moment'

export const getCurrentUser = () => {
  if (typeof window !== 'undefined') {
    const cookies = new Cookies()
    const token = cookies.get('jwtToken')
    if (token) {
      const user = jwtDecode(cookies['jwtToken'])
      return user
    }
    return null
  }

  return null
}

export const saveNewTokenAndReturnUser = (newToken) => {
  const cookies = new Cookies()
  
  cookies.set('jwtToken', newToken, { path: '/', expires: moment().add(365, 'days').toDate()  })
  const user = jwtDecode(newToken)
  return user
}
