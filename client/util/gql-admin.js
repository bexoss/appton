import gql from 'graphql-tag'

export const GET_DOCUMENTS = gql`
  query GetDocuments($collection: String!, $page: Int, $search: String, $filter: String, $perPage: Int, $mid: String) {
    getDocuments(collection: $collection, page: $page, search: $search, filter: $filter, perPage: $perPage, mid: $mid) {
      ... on User {
        id
        name
        email
        level
        company
        mobile
        createdAt
        levelExpiredAt
      }
      ... on Post {
        id
        title
        content
        user {
          name
          email
        }
        comments {
          _id
        }
        createdAt
        status
        isNotice
        isSecret
        viewCount
      }
      ... on Subscription {
        id
        name
        uid
        service
        mobile
        createdAt
        email
        nextPayment
        nextPaymentAmount
        alive
        payments {
          createdAt
          amount
        }
      }
      ... on Comment {
        _id
        user {
          email
          name
        }
        createdAt
        updatedAt
        content
        mid
        parentDocument
      }
      ... on Message {
        id
        createdAt
        content
        success_cnt
        msg_type
        type
        cost
      }
      ... on Cosmetics {
        id
        productName
        productCode
        brand
        ingredients
        seller
        madeIn
        madeBy
        image
        description
        url
      }
      ... on Ingredient {
        id
        nameKor
        INCI
        skinDeepUrl
      }
      ... on Stat {
        id
        date
        visitors
        pageviews
        sessions
        usersCount
        hit {
          ip
          product
          userEmail
        }
      }
      ... on Order {
        id
        buyer
        buyerName
        buyerContact
        buyerEmail
        product
        createdAt
        isMoq
        isSample
        weight
        status
      }
    }
  }
`

export const GET_COUNT = gql`
  query GetCount($collection: String!, $search: String, $filter: String, $mid: String) {
    getCount(collection: $collection, search: $search, filter: $filter, mid: $mid)
  }
`

export const RAW_UPDATE = gql`
  mutation RawUpdate($collection: String!, $data: String!) {
    rawUpdate(collection: $collection, data: $data)
  }
`

export const ADD_DOCUMENT = gql`
  mutation AddDocument($collection: String!, $addInput: AddInput) {
    addDocument(collection: $collection, addInput: $addInput) {
      ... on User {
        id
        name
        email
        level
        mobile
        createdAt
      }
      ... on Subscription {
        id
        email
        name
        mobile
        nextPayment
        nextPaymentAmount
        payments {
          createdAt
          amount
        }
        uid
        alive
        createdAt
        service
      }
    }
  }
`

export const UPDATE_DOCUMENT = gql`
  mutation UpdateDocument($collection: String!, $ids: [String!]!, $updateField: String!, $updateValue: String!) {
    updateDocument(collection: $collection, ids: $ids, updateField: $updateField, updateValue: $updateValue)
  }
`

export const REMOVE_DOCUMENT = gql`
  mutation RemoveDocument($collection: String!, $ids: [String!]!) {
    removeDocument(collection: $collection, ids: $ids)
  }
`

export const SEND_SMS = gql`
  mutation SendSMS($sender: String!, $target: String!, $content: String!, $receiver: String) {
    sendSMS(sender: $sender, target: $target, content: $content, receiver: $receiver) {
      id
      content
      type
      msg_type
      success_cnt
    }
  }
`

export const GET_USERS_COUNT = gql`
  query GetUsersCount {
    getUsersCount {
      all
      subs
      normals
    }
  }
`

export const GET_TODAY_STATS = gql`
  query GetTodayStats {
    getTodayStats {
      visitors
      sessions
      pageviews
    }
  }
`

export const GET_STATS = gql`
  query GetStats {
    getStats {
      id
      date
      sessions
      pageviews
      usersCount
      inOrder
      hit {
        ip
        product
      }
    }
  }
`
export const GET_ORDERS_ADMIN = gql`
  query GetOrdersAdmin {
    getOrdersAdmin {
      id
      buyer
      buyerName
      buyerContact
      buyerEmail
      note
      product
      weight
      isMoq
      status
      createdAt
      quotes {
        createdAt
        seller
        sellerName
        sellerContact
        price
        unit
        product
      }
    }
  }
`
