import React from 'react'
import { AuthProvider } from '../context/auth'
import Layout from '../components/Layout'
import { useRouter } from 'next/router'
import 'normalize.css'
import 'semantic-ui-css/semantic.min.css'
import 'react-quill/dist/quill.snow.css'
import '../components/css/global.css'
import Head from 'next/head'

function Default({ Component, pageProps, mobile }) {
  const router = useRouter()
  return (
    <AuthProvider>
      <Head>
        <title>주식회사 앱톤</title>
      </Head>

      <Layout>
        <Component {...pageProps} />
      </Layout>
    </AuthProvider>
  )
}

export default Default
