import Document, {  Head, Main, NextScript } from 'next/document'
import { ServerStyleSheet } from 'styled-components'

export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const sheet = new ServerStyleSheet()
    const originalRenderPage = ctx.renderPage

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) => sheet.collectStyles(<App {...props} />),
        })

      const initialProps = await Document.getInitialProps(ctx)
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      }
    } finally {
      sheet.seal()
    }
  }

  render() {
    return (
      <html lang='ko'>
        <Head>
          <meta name='viewport' content='width=device-width, initial-scale=1.0' />
          <meta property='og:locale' content='ko_KR' />
          <meta property='og:site_name' content='앱톤' />
          <meta name='description' content='주식회사 앱톤' />
          <meta property='og:type' content='website' />
          <meta property='og:title' content='앱톤' />
          <meta property='og:description' content='주식회사 앱톤' />
          <meta property='og:url' content='https://appton.co.kr' />
          <link rel='canonical' href='https://appton.co.kr' />
          <link rel='icon' type='image/x-icon' href='/favicon.ico' />
          <link href='//spoqa.github.io/spoqa-han-sans/css/SpoqaHanSans-kr.css' rel='stylesheet' type='text/css' />
          <link href="https://fonts.googleapis.com/css2?family=Kanit&display=swap" rel="stylesheet">
            
          </link>
        </Head>
        <body class="Site">
          <Main class="Site-content"/>
          <NextScript />
        </body>
      </html>
    )
  }
}
