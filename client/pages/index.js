import { Container, Segment, Header, Button, Grid, Image, Divider } from 'semantic-ui-react'
import { DesktopOnly } from '../components/Styles/Layout'
import GoogleMapReact from 'google-map-react'

function Index() {
  return (
    <div>
      <div className='hero'>
        <Container text style={{ marginTop: 40, padding: 50 }} className='mobile_narrow'>
          <h2 className='sans'>안녕하세요!</h2>
          <p>앱톤 홈페이지에 오신 것을 환영합니다. 앱톤은 웹앱, 모바일앱을 개발하는 소프트웨어 개발 회사입니다. 하지만 외주 개발 보다는 자체 서비스 개발을 주로 해 왔습니다.</p>
          <p>
            틀에 박힌 서비스 보다는 필요하지만 세상에 없는 서비스를 만들고 싶습니다. 또한 이왕 만드는 것 우리가 없어지더라도 서비스 만큼은 계속해서 유지되었으면 합니다. 이를 위해서
            초기 서비스 기획 단계에서부터 사회적 편익과 영속성을 최우선으로 삼습니다.
          </p>
          <p>
            호랑이는 죽어서 가죽을 남기고, 앱톤은 죽어서 수백 개의 플랫폼 서비스를 남기고자 합니다. 우리가 좋아하는 단어는 "기본", "핵심", "효율", "자동화" 등 입니다. 우리는 과대
            포장과 속임수가 넘치는 사회에서 기본에 충실한 본질만 가지고도 성공할 수 있다는 것을 증명하고 싶습니다.
          </p>
        </Container>

        <Container text style={{ padding: 50, paddingTop: 10 }} className='mobile_narrow'>
          <h2 className='sans'>운영중인 서비스</h2>
          <Segment secondary>
            <div>
              <h2>COOS (쿠스)</h2>{' '}
              <a href='https://coos.kr' target='_blank'>
                https://coos.kr
              </a>
            </div>
            <p style={{ marginTop: 10, padding: 20, paddingTop: 5 }}>
              쿠스는 화장품 성분 조회 플랫폼입니다. 화장품 제조사 연구원들이 성분 정보를 조회하는데 여러 개의 웹사이트를 돌아다니며 불편하게 자료를 검색하는 것을 개선하고자
              제작하였습니다. 오픈 이후 관련 업계 수많은 기업에서 사용중입니다.
            </p>
          </Segment>
          {/* <Segment tertiary>
            <div>
              <h2>GRIP (그립)</h2>{' '}
              <a href='https://grip.kr' target='_blank'>
                https://grip.kr
              </a>
            </div>
            <p style={{ marginTop: 10, padding: 20, paddingTop: 5 }}>
              그립은 스포츠 동호회용 회원 관리 플랫폼입니다. 회원들이 수백 명이 넘는데 엑셀 파일로 일일히 작업하고 주고 받는 것이 많은 불편을 초래해 클라우드 환경에서 회원 관리를
              할 수 있게 제작되었습니다. 현재 베타 서비스 중입니다.
            </p>
          </Segment> */}
        </Container>

        <Container text style={{ padding: 50, paddingTop: 10, marginBottom: 50 }} className='mobile_narrow'>
          <h2 className='sans'>따뜻한 차 한 잔 하실래요?</h2>
          <div style={{ height: '300px', width: '100%' }}>
            <GoogleMapReact
              bootstrapURLKeys={{ key: 'AIzaSyAXFPJOZNGr6CCb8L3_blCKn_hnbGLXPzs' }}
              defaultCenter={{
                lat: 37.570791,
                lng: 126.983585,
              }}
              defaultZoom={17}
              onGoogleApiLoaded={({ map, maps }) =>
                new maps.Marker({
                  position: {
                    lat: 37.570791,
                    lng: 126.983585,
                  },
                  map,
                  title: 'Hello World!',
                })
              }
            ></GoogleMapReact>
          </div>

          <p style={{ marginTop: 20 }}>
            저희와 따뜻한 차 한 잔 하며 이야기 하실까요? 저희 회사는 서울 종로에 위치합니다. 미팅을 원하신다면 이메일 연락 또는 전화 연락을 주세요. 기다리고 있겠습니다.
          </p>
        </Container>
      </div>
    </div>
  )
}

export default Index
