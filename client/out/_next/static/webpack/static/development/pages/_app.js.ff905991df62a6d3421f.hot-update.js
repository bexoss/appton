webpackHotUpdate("static\\development\\pages\\_app.js",{

/***/ "./components/Layout/index.js":
/*!************************************!*\
  !*** ./components/Layout/index.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _context_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../context/auth */ "./context/auth.js");
/* harmony import */ var semantic_ui_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! semantic-ui-react */ "./node_modules/semantic-ui-react/dist/es/index.js");
/* harmony import */ var _Styles_Layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Styles/Layout */ "./components/Styles/Layout.js");
var _jsxFileName = "C:\\Users\\COM\\Projects\\appton\\client\\components\\Layout\\index.js",
    _s = $RefreshSig$();

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;







function Layout(_ref) {
  _s();

  var mobile = _ref.mobile,
      children = _ref.children;
  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"])();
  var context = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_context_auth__WEBPACK_IMPORTED_MODULE_3__["AuthContext"]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (context && router) {
      if (context.user) {
        if (router.pathname === '/login' || router.pathname === '/register') {
          router.push('/');
        }
      } else {
        if (router.pathname === '/profile') {
          router.push('/');
        }
      }
    }
  }, []);
  return __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Site"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 5
    }
  }, __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Header"], {
    isIndex: router.pathname === '/',
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }
  }, __jsx(semantic_ui_react__WEBPACK_IMPORTED_MODULE_4__["Container"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 9
    }
  }, __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["HeaderRow"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 11
    }
  }, __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["LogoWrapper"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 13
    }
  }, __jsx("span", {
    className: "kanit",
    style: {
      color: '#222'
    },
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 15
    }
  }, "Appton Inc."))))), __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Main"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 7
    }
  }, __jsx("div", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 9
    }
  }, children)), __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Footer"], {
    className: "sans",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 7
    }
  }));
}

_s(Layout, "YO1h8rPTZkc6xHy66nmNQ0d7s7s=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"]];
});

_c = Layout;
/* harmony default export */ __webpack_exports__["default"] = (Layout);

var _c;

$RefreshReg$(_c, "Layout");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports_1 = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports_1, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports_1)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports_1;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports_1)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL0xheW91dC9pbmRleC5qcyJdLCJuYW1lcyI6WyJMYXlvdXQiLCJtb2JpbGUiLCJjaGlsZHJlbiIsInJvdXRlciIsInVzZVJvdXRlciIsImNvbnRleHQiLCJ1c2VDb250ZXh0IiwiQXV0aENvbnRleHQiLCJ1c2VFZmZlY3QiLCJ1c2VyIiwicGF0aG5hbWUiLCJwdXNoIiwiY29sb3IiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTQSxNQUFULE9BQXNDO0FBQUE7O0FBQUEsTUFBcEJDLE1BQW9CLFFBQXBCQSxNQUFvQjtBQUFBLE1BQVpDLFFBQVksUUFBWkEsUUFBWTtBQUNwQyxNQUFNQyxNQUFNLEdBQUdDLDZEQUFTLEVBQXhCO0FBQ0EsTUFBTUMsT0FBTyxHQUFHQyx3REFBVSxDQUFDQyx5REFBRCxDQUExQjtBQUVBQyx5REFBUyxDQUFDLFlBQU07QUFDZCxRQUFJSCxPQUFPLElBQUlGLE1BQWYsRUFBdUI7QUFDckIsVUFBSUUsT0FBTyxDQUFDSSxJQUFaLEVBQWtCO0FBQ2hCLFlBQUlOLE1BQU0sQ0FBQ08sUUFBUCxLQUFvQixRQUFwQixJQUFnQ1AsTUFBTSxDQUFDTyxRQUFQLEtBQW9CLFdBQXhELEVBQXFFO0FBQ25FUCxnQkFBTSxDQUFDUSxJQUFQLENBQVksR0FBWjtBQUNEO0FBQ0YsT0FKRCxNQUlPO0FBQ0wsWUFBSVIsTUFBTSxDQUFDTyxRQUFQLEtBQW9CLFVBQXhCLEVBQW9DO0FBQ2xDUCxnQkFBTSxDQUFDUSxJQUFQLENBQVksR0FBWjtBQUNEO0FBQ0Y7QUFDRjtBQUNGLEdBWlEsRUFZTixFQVpNLENBQVQ7QUFjQSxTQUNFLE1BQUMsbURBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFLE1BQUMscURBQUQ7QUFBUSxXQUFPLEVBQUVSLE1BQU0sQ0FBQ08sUUFBUCxLQUFvQixHQUFyQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQywyREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQyx3REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQywwREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0U7QUFBTSxhQUFTLEVBQUMsT0FBaEI7QUFBd0IsU0FBSyxFQUFFO0FBQUVFLFdBQUssRUFBRTtBQUFULEtBQS9CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREYsQ0FERixDQURGLENBREYsQ0FERixFQWFFLE1BQUMsbURBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBTVYsUUFBTixDQURGLENBYkYsRUFpQkUsTUFBQyxxREFBRDtBQUFRLGFBQVMsRUFBQyxNQUFsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLElBakJGLENBREY7QUE2Q0Q7O0dBL0RRRixNO1VBQ1FJLHFEOzs7S0FEUkosTTtBQWlFTUEscUVBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svc3RhdGljXFxkZXZlbG9wbWVudFxccGFnZXNcXF9hcHAuanMuZmY5MDU5OTFkZjYyYTZkMzQyMWYuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0LCB1c2VDb250ZXh0IH0gZnJvbSAncmVhY3QnXHJcbmltcG9ydCBMaW5rIGZyb20gJ25leHQvbGluaydcclxuaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSAnbmV4dC9yb3V0ZXInXHJcbmltcG9ydCB7IEF1dGhDb250ZXh0IH0gZnJvbSAnLi4vLi4vY29udGV4dC9hdXRoJ1xyXG5pbXBvcnQgeyBDb250YWluZXIgfSBmcm9tICdzZW1hbnRpYy11aS1yZWFjdCdcclxuaW1wb3J0IHsgU2l0ZSwgSGVhZGVyLCBMb2dvV3JhcHBlciwgSGVhZGVyUm93LCBNYWluLCBGb290ZXIgfSBmcm9tICcuLi9TdHlsZXMvTGF5b3V0J1xyXG5cclxuZnVuY3Rpb24gTGF5b3V0KHsgbW9iaWxlLCBjaGlsZHJlbiB9KSB7XHJcbiAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKClcclxuICBjb25zdCBjb250ZXh0ID0gdXNlQ29udGV4dChBdXRoQ29udGV4dClcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmIChjb250ZXh0ICYmIHJvdXRlcikge1xyXG4gICAgICBpZiAoY29udGV4dC51c2VyKSB7XHJcbiAgICAgICAgaWYgKHJvdXRlci5wYXRobmFtZSA9PT0gJy9sb2dpbicgfHwgcm91dGVyLnBhdGhuYW1lID09PSAnL3JlZ2lzdGVyJykge1xyXG4gICAgICAgICAgcm91dGVyLnB1c2goJy8nKVxyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBpZiAocm91dGVyLnBhdGhuYW1lID09PSAnL3Byb2ZpbGUnKSB7XHJcbiAgICAgICAgICByb3V0ZXIucHVzaCgnLycpXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfSwgW10pXHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8U2l0ZT5cclxuICAgICAgPEhlYWRlciBpc0luZGV4PXtyb3V0ZXIucGF0aG5hbWUgPT09ICcvJ30+XHJcbiAgICAgICAgPENvbnRhaW5lcj5cclxuICAgICAgICAgIDxIZWFkZXJSb3c+XHJcbiAgICAgICAgICAgIDxMb2dvV3JhcHBlcj5cclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9J2thbml0JyBzdHlsZT17eyBjb2xvcjogJyMyMjInIH19PlxyXG4gICAgICAgICAgICAgICAgQXBwdG9uIEluYy5cclxuICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgIDwvTG9nb1dyYXBwZXI+XHJcbiAgICAgICAgICA8L0hlYWRlclJvdz5cclxuICAgICAgICA8L0NvbnRhaW5lcj5cclxuICAgICAgPC9IZWFkZXI+XHJcblxyXG4gICAgICA8TWFpbj5cclxuICAgICAgICA8ZGl2PntjaGlsZHJlbn08L2Rpdj5cclxuICAgICAgPC9NYWluPlxyXG5cclxuICAgICAgPEZvb3RlciBjbGFzc05hbWU9J3NhbnMnPlxyXG4gICAgICAgIHsvKiA8U2VnbWVudFxyXG4gICAgICAgICAgdmVydGljYWxcclxuICAgICAgICAgIHN0eWxlPXt7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6ICcxZW0gMGVtJyxcclxuICAgICAgICAgICAgYm9yZGVyVG9wOiAnMXB4IHNvbGlkICNlZWUnLFxyXG4gICAgICAgICAgICBmb250U2l6ZTogJzAuOXJlbScsXHJcbiAgICAgICAgICB9fVxyXG4gICAgICAgID5cclxuICAgICAgICAgIDxDb250YWluZXIgdGV4dD5cclxuICAgICAgICAgICAgPEdyaWQgZGl2aWRlZCBzdGFja2FibGU+XHJcbiAgICAgICAgICAgICAgPEdyaWQuUm93PlxyXG4gICAgICAgICAgICAgICAgPEdyaWQuQ29sdW1uIHdpZHRoPXsxNn0+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3sgY29sb3I6ICcjYWFhJywgbGluZUhlaWdodDogJzEuOHJlbScsIGZvbnRTaXplOiAnMXJlbScgfX0gY2xhc3NOYW1lPSdzYW5zJz5cclxuICAgICAgICAgICAgICAgICAgICAo7KO8KeyVse2GpCA8UmVzcG9uc2l2ZVNlcGFyYXRvcj58PC9SZXNwb25zaXZlU2VwYXJhdG9yPiDshJzsmrjsi5wg7KKF66Gc6rWsIOyiheuhnCA1MSDsooXroZztg4Dsm4wgMTjsuLVcclxuICAgICAgICAgICAgICAgICAgICA8YnIgLz5cclxuICAgICAgICAgICAgICAgICAgICDsgqzsl4XsnpDrk7HroZ3rsojtmLggODk3LTg3LTAxNTE2IDxSZXNwb25zaXZlU2VwYXJhdG9yPnw8L1Jlc3BvbnNpdmVTZXBhcmF0b3I+64yA7ZGc7J6QIOq5gOykgO2DnCA8UmVzcG9uc2l2ZVNlcGFyYXRvcj58PC9SZXNwb25zaXZlU2VwYXJhdG9yPlxyXG4gICAgICAgICAgICAgICAgICAgIDxSZXNwb25zaXZlQlIgLz4g7KCE7ZmUIDE1NDQtMzE5NyB8IOydtOuplOydvCBjc0BhcHB0b24uY28ua3JcclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L0dyaWQuQ29sdW1uPlxyXG4gICAgICAgICAgICAgIDwvR3JpZC5Sb3c+XHJcbiAgICAgICAgICAgIDwvR3JpZD5cclxuICAgICAgICAgIDwvQ29udGFpbmVyPlxyXG4gICAgICAgIDwvU2VnbWVudD4gKi99XHJcbiAgICAgIDwvRm9vdGVyPlxyXG4gICAgPC9TaXRlPlxyXG4gIClcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgTGF5b3V0XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=