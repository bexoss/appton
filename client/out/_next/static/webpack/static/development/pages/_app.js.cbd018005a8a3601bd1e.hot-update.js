webpackHotUpdate("static\\development\\pages\\_app.js",{

/***/ "./components/Styles/Layout.js":
/*!*************************************!*\
  !*** ./components/Styles/Layout.js ***!
  \*************************************/
/*! exports provided: headerHeight, mobileHeaderHeight, headerBorderBottomColor, mainBackgroundColor, headerBackgroundColor, Site, Header, Main, HeaderRow, LogoWrapper, Footer, SideMenuWrapper, SideBarWrapper, PaperTitle, ResponsiveBR, ResponsiveSeparator, ResponsiveContainer, ResponsiveSegment, HorizontalSpaceBetween, SmallPaper, DesktopOnly, MobileOnly, SideMenu */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "headerHeight", function() { return headerHeight; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mobileHeaderHeight", function() { return mobileHeaderHeight; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "headerBorderBottomColor", function() { return headerBorderBottomColor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mainBackgroundColor", function() { return mainBackgroundColor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "headerBackgroundColor", function() { return headerBackgroundColor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Site", function() { return Site; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Header", function() { return Header; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Main", function() { return Main; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderRow", function() { return HeaderRow; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoWrapper", function() { return LogoWrapper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Footer", function() { return Footer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SideMenuWrapper", function() { return SideMenuWrapper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SideBarWrapper", function() { return SideBarWrapper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaperTitle", function() { return PaperTitle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResponsiveBR", function() { return ResponsiveBR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResponsiveSeparator", function() { return ResponsiveSeparator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResponsiveContainer", function() { return ResponsiveContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResponsiveSegment", function() { return ResponsiveSegment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HorizontalSpaceBetween", function() { return HorizontalSpaceBetween; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmallPaper", function() { return SmallPaper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DesktopOnly", function() { return DesktopOnly; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MobileOnly", function() { return MobileOnly; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SideMenu", function() { return SideMenu; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var semantic_ui_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! semantic-ui-react */ "./node_modules/semantic-ui-react/dist/es/index.js");


var headerHeight = 88;
var mobileHeaderHeight = 80;
var headerBorderBottomColor = '#EFEFF4';
var mainBackgroundColor = '#fff';
var headerBackgroundColor = '#F7F7F7';
var Site = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div.withConfig({
  displayName: "Layout__Site",
  componentId: "sc-11sur5a-0"
})(["flex-direction:column;"]);
var Header = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div.withConfig({
  displayName: "Layout__Header",
  componentId: "sc-11sur5a-1"
})(["width:100%;height:70px;display:flex;flex-direction:row;align-items:center;background:#f4f5f7;@media (max-width:767px){color:#000;}"]);
var Main = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].main.withConfig({
  displayName: "Layout__Main",
  componentId: "sc-11sur5a-2"
})(["flex:1;"]);
var HeaderRow = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div.withConfig({
  displayName: "Layout__HeaderRow",
  componentId: "sc-11sur5a-3"
})(["display:flex;flex-direction:row;align-items:center;"]);
var LogoWrapper = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div.withConfig({
  displayName: "Layout__LogoWrapper",
  componentId: "sc-11sur5a-4"
})(["width:116px;font-size:20px;color:#000;font-family:'Lexend Tera',sans-serif;font-weight:600;font-size:1.6rem;@media (max-width:767px){margin-left:5px;}"]);
var Footer = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div.withConfig({
  displayName: "Layout__Footer",
  componentId: "sc-11sur5a-5"
})([""]);
var SideMenuWrapper = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div.withConfig({
  displayName: "Layout__SideMenuWrapper",
  componentId: "sc-11sur5a-6"
})(["height:48px;display:flex;flex-direction:column;justify-content:center;background:#fff;border-bottom:1px solid #eee;"]);
var SideBarWrapper = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div.withConfig({
  displayName: "Layout__SideBarWrapper",
  componentId: "sc-11sur5a-7"
})([""]);
var PaperTitle = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].h2.withConfig({
  displayName: "Layout__PaperTitle",
  componentId: "sc-11sur5a-8"
})(["font-size:1.8rem;letter-spacing:-1px;margin:0;"]);
var ResponsiveBR = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div.withConfig({
  displayName: "Layout__ResponsiveBR",
  componentId: "sc-11sur5a-9"
})(["display:inline-block;@media (max-width:767px){display:block;}"]);
var ResponsiveSeparator = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].span.withConfig({
  displayName: "Layout__ResponsiveSeparator",
  componentId: "sc-11sur5a-10"
})(["margin:0px 4px;color:#ccc;", ""], function (props) {
  return props.hideMobile && Object(styled_components__WEBPACK_IMPORTED_MODULE_0__["css"])(["@media (max-width:767px){display:none;}"]);
});
var ResponsiveContainer = Object(styled_components__WEBPACK_IMPORTED_MODULE_0__["default"])(semantic_ui_react__WEBPACK_IMPORTED_MODULE_1__["Container"]).withConfig({
  displayName: "Layout__ResponsiveContainer",
  componentId: "sc-11sur5a-11"
})(["@media (max-width:767px){margin:0 !important;margin-top:10px !important;padding:0 !important;}"]);
var ResponsiveSegment = Object(styled_components__WEBPACK_IMPORTED_MODULE_0__["default"])(semantic_ui_react__WEBPACK_IMPORTED_MODULE_1__["Segment"]).withConfig({
  displayName: "Layout__ResponsiveSegment",
  componentId: "sc-11sur5a-12"
})(["@media (max-width:767px){padding:10px !important;}"]);
var HorizontalSpaceBetween = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div.withConfig({
  displayName: "Layout__HorizontalSpaceBetween",
  componentId: "sc-11sur5a-13"
})(["display:flex;flex-direction:row;justify-content:space-between;align-items:center;"]);
var SmallPaper = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div.withConfig({
  displayName: "Layout__SmallPaper",
  componentId: "sc-11sur5a-14"
})(["display:flex;flex-direction:column;justify-content:center;max-width:450px;background:#fff;padding:30px;margin:0 auto;margin-top:120px;min-height:300px;box-shadow:0 1px 2px 0 rgba(34,36,38,0.15);@media (max-width:767px){margin-top:30px;-webkit-box-shadow:none !important;-moz-box-shadow:none !important;box-shadow:none !important;}"]);
var DesktopOnly = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div.withConfig({
  displayName: "Layout__DesktopOnly",
  componentId: "sc-11sur5a-15"
})(["@media (max-width:767px){display:none;}"]);
var MobileOnly = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div.withConfig({
  displayName: "Layout__MobileOnly",
  componentId: "sc-11sur5a-16"
})(["display:none;@media (max-width:767px){display:flex;}"]);
var SideMenu = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div.withConfig({
  displayName: "Layout__SideMenu",
  componentId: "sc-11sur5a-17"
})(["text-decoration:none;padding:15px;font-size:1rem;cursor:pointer;", ""], function (props) {
  return props.active && Object(styled_components__WEBPACK_IMPORTED_MODULE_0__["css"])(["font-weight:600;"]);
});

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports_1 = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports_1, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports_1)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports_1;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports_1)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL1N0eWxlcy9MYXlvdXQuanMiXSwibmFtZXMiOlsiaGVhZGVySGVpZ2h0IiwibW9iaWxlSGVhZGVySGVpZ2h0IiwiaGVhZGVyQm9yZGVyQm90dG9tQ29sb3IiLCJtYWluQmFja2dyb3VuZENvbG9yIiwiaGVhZGVyQmFja2dyb3VuZENvbG9yIiwiU2l0ZSIsInN0eWxlZCIsImRpdiIsIkhlYWRlciIsIk1haW4iLCJtYWluIiwiSGVhZGVyUm93IiwiTG9nb1dyYXBwZXIiLCJGb290ZXIiLCJTaWRlTWVudVdyYXBwZXIiLCJTaWRlQmFyV3JhcHBlciIsIlBhcGVyVGl0bGUiLCJoMiIsIlJlc3BvbnNpdmVCUiIsIlJlc3BvbnNpdmVTZXBhcmF0b3IiLCJzcGFuIiwicHJvcHMiLCJoaWRlTW9iaWxlIiwiY3NzIiwiUmVzcG9uc2l2ZUNvbnRhaW5lciIsIkNvbnRhaW5lciIsIlJlc3BvbnNpdmVTZWdtZW50IiwiU2VnbWVudCIsIkhvcml6b250YWxTcGFjZUJldHdlZW4iLCJTbWFsbFBhcGVyIiwiRGVza3RvcE9ubHkiLCJNb2JpbGVPbmx5IiwiU2lkZU1lbnUiLCJhY3RpdmUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVPLElBQU1BLFlBQVksR0FBRyxFQUFyQjtBQUNBLElBQU1DLGtCQUFrQixHQUFHLEVBQTNCO0FBQ0EsSUFBTUMsdUJBQXVCLEdBQUcsU0FBaEM7QUFDQSxJQUFNQyxtQkFBbUIsR0FBRyxNQUE1QjtBQUNBLElBQU1DLHFCQUFxQixHQUFHLFNBQTlCO0FBRUEsSUFBTUMsSUFBSSxHQUFHQyx5REFBTSxDQUFDQyxHQUFWO0FBQUE7QUFBQTtBQUFBLDhCQUFWO0FBR0EsSUFBTUMsTUFBTSxHQUFHRix5REFBTSxDQUFDQyxHQUFWO0FBQUE7QUFBQTtBQUFBLDBJQUFaO0FBWUEsSUFBTUUsSUFBSSxHQUFHSCx5REFBTSxDQUFDSSxJQUFWO0FBQUE7QUFBQTtBQUFBLGVBQVY7QUFJQSxJQUFNQyxTQUFTLEdBQUdMLHlEQUFNLENBQUNDLEdBQVY7QUFBQTtBQUFBO0FBQUEsMkRBQWY7QUFPQSxJQUFNSyxXQUFXLEdBQUdOLHlEQUFNLENBQUNDLEdBQVY7QUFBQTtBQUFBO0FBQUEsOEpBQWpCO0FBY0EsSUFBTU0sTUFBTSxHQUFHUCx5REFBTSxDQUFDQyxHQUFWO0FBQUE7QUFBQTtBQUFBLFFBQVo7QUFJQSxJQUFNTyxlQUFlLEdBQUdSLHlEQUFNLENBQUNDLEdBQVY7QUFBQTtBQUFBO0FBQUEsMkhBQXJCO0FBU0EsSUFBTVEsY0FBYyxHQUFHVCx5REFBTSxDQUFDQyxHQUFWO0FBQUE7QUFBQTtBQUFBLFFBQXBCO0FBRUEsSUFBTVMsVUFBVSxHQUFHVix5REFBTSxDQUFDVyxFQUFWO0FBQUE7QUFBQTtBQUFBLHNEQUFoQjtBQU1BLElBQU1DLFlBQVksR0FBR1oseURBQU0sQ0FBQ0MsR0FBVjtBQUFBO0FBQUE7QUFBQSxxRUFBbEI7QUFPQSxJQUFNWSxtQkFBbUIsR0FBR2IseURBQU0sQ0FBQ2MsSUFBVjtBQUFBO0FBQUE7QUFBQSx1Q0FHNUIsVUFBQ0MsS0FBRDtBQUFBLFNBQ0FBLEtBQUssQ0FBQ0MsVUFBTixJQUNBQyw2REFEQSw2Q0FEQTtBQUFBLENBSDRCLENBQXpCO0FBWUEsSUFBTUMsbUJBQW1CLEdBQUdsQixpRUFBTSxDQUFDbUIsMkRBQUQsQ0FBVDtBQUFBO0FBQUE7QUFBQSxzR0FBekI7QUFRQSxJQUFNQyxpQkFBaUIsR0FBR3BCLGlFQUFNLENBQUNxQix5REFBRCxDQUFUO0FBQUE7QUFBQTtBQUFBLDBEQUF2QjtBQVNBLElBQU1DLHNCQUFzQixHQUFHdEIseURBQU0sQ0FBQ0MsR0FBVjtBQUFBO0FBQUE7QUFBQSx5RkFBNUI7QUFPQSxJQUFNc0IsVUFBVSxHQUFHdkIseURBQU0sQ0FBQ0MsR0FBVjtBQUFBO0FBQUE7QUFBQSxrVkFBaEI7QUFvQkEsSUFBTXVCLFdBQVcsR0FBR3hCLHlEQUFNLENBQUNDLEdBQVY7QUFBQTtBQUFBO0FBQUEsK0NBQWpCO0FBTUEsSUFBTXdCLFVBQVUsR0FBR3pCLHlEQUFNLENBQUNDLEdBQVY7QUFBQTtBQUFBO0FBQUEsNERBQWhCO0FBT0EsSUFBTXlCLFFBQVEsR0FBRzFCLHlEQUFNLENBQUNDLEdBQVY7QUFBQTtBQUFBO0FBQUEsNkVBTWpCLFVBQUNjLEtBQUQ7QUFBQSxTQUNBQSxLQUFLLENBQUNZLE1BQU4sSUFDQVYsNkRBREEsc0JBREE7QUFBQSxDQU5pQixDQUFkIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3N0YXRpY1xcZGV2ZWxvcG1lbnRcXHBhZ2VzXFxfYXBwLmpzLmNiZDAxODAwNWE4YTM2MDFiZDFlLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgc3R5bGVkLCB7IGNzcyB9IGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJ1xyXG5pbXBvcnQgeyBDb250YWluZXIsIFNlZ21lbnQgfSBmcm9tICdzZW1hbnRpYy11aS1yZWFjdCdcclxuXHJcbmV4cG9ydCBjb25zdCBoZWFkZXJIZWlnaHQgPSA4OFxyXG5leHBvcnQgY29uc3QgbW9iaWxlSGVhZGVySGVpZ2h0ID0gODBcclxuZXhwb3J0IGNvbnN0IGhlYWRlckJvcmRlckJvdHRvbUNvbG9yID0gJyNFRkVGRjQnXHJcbmV4cG9ydCBjb25zdCBtYWluQmFja2dyb3VuZENvbG9yID0gJyNmZmYnXHJcbmV4cG9ydCBjb25zdCBoZWFkZXJCYWNrZ3JvdW5kQ29sb3IgPSAnI0Y3RjdGNydcclxuXHJcbmV4cG9ydCBjb25zdCBTaXRlID0gc3R5bGVkLmRpdmBcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5gXHJcbmV4cG9ydCBjb25zdCBIZWFkZXIgPSBzdHlsZWQuZGl2YFxyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogNzBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBiYWNrZ3JvdW5kOiAjZjRmNWY3O1xyXG4gIEBtZWRpYSAobWF4LXdpZHRoOiA3NjdweCkge1xyXG4gICAgY29sb3I6ICMwMDA7XHJcbiAgfVxyXG5gXHJcblxyXG5leHBvcnQgY29uc3QgTWFpbiA9IHN0eWxlZC5tYWluYFxyXG4gIGZsZXg6IDE7XHJcbmBcclxuXHJcbmV4cG9ydCBjb25zdCBIZWFkZXJSb3cgPSBzdHlsZWQuZGl2YFxyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5gXHJcblxyXG5cclxuZXhwb3J0IGNvbnN0IExvZ29XcmFwcGVyID0gc3R5bGVkLmRpdmBcclxuICB3aWR0aDogMTE2cHg7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gIGNvbG9yOiAjMDAwO1xyXG4gIGZvbnQtZmFtaWx5OiAnTGV4ZW5kIFRlcmEnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgZm9udC1zaXplOiAxLjZyZW07XHJcbiAgQG1lZGlhIChtYXgtd2lkdGg6IDc2N3B4KSB7XHJcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xyXG4gIH1cclxuYFxyXG5cclxuXHJcblxyXG5leHBvcnQgY29uc3QgRm9vdGVyID0gc3R5bGVkLmRpdmBcclxuICBcclxuYFxyXG5cclxuZXhwb3J0IGNvbnN0IFNpZGVNZW51V3JhcHBlciA9IHN0eWxlZC5kaXZgXHJcbiAgaGVpZ2h0OiA0OHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWVlO1xyXG5gXHJcblxyXG5leHBvcnQgY29uc3QgU2lkZUJhcldyYXBwZXIgPSBzdHlsZWQuZGl2YGBcclxuXHJcbmV4cG9ydCBjb25zdCBQYXBlclRpdGxlID0gc3R5bGVkLmgyYFxyXG4gIGZvbnQtc2l6ZTogMS44cmVtO1xyXG4gIGxldHRlci1zcGFjaW5nOiAtMXB4O1xyXG4gIG1hcmdpbjogMDtcclxuYFxyXG5cclxuZXhwb3J0IGNvbnN0IFJlc3BvbnNpdmVCUiA9IHN0eWxlZC5kaXZgXHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIEBtZWRpYSAobWF4LXdpZHRoOiA3NjdweCkge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgfVxyXG5gXHJcblxyXG5leHBvcnQgY29uc3QgUmVzcG9uc2l2ZVNlcGFyYXRvciA9IHN0eWxlZC5zcGFuYFxyXG4gIG1hcmdpbjogMHB4IDRweDtcclxuICBjb2xvcjogI2NjYztcclxuICAkeyhwcm9wcykgPT5cclxuICAgIHByb3BzLmhpZGVNb2JpbGUgJiZcclxuICAgIGNzc2BcclxuICAgICAgQG1lZGlhIChtYXgtd2lkdGg6IDc2N3B4KSB7XHJcbiAgICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgICAgfVxyXG4gICAgYH1cclxuYFxyXG5cclxuZXhwb3J0IGNvbnN0IFJlc3BvbnNpdmVDb250YWluZXIgPSBzdHlsZWQoQ29udGFpbmVyKWBcclxuICBAbWVkaWEgKG1heC13aWR0aDogNzY3cHgpIHtcclxuICAgIG1hcmdpbjogMCAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luLXRvcDogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xyXG4gIH1cclxuYFxyXG5cclxuZXhwb3J0IGNvbnN0IFJlc3BvbnNpdmVTZWdtZW50ID0gc3R5bGVkKFNlZ21lbnQpYFxyXG4gIEBtZWRpYSAobWF4LXdpZHRoOiA3NjdweCkge1xyXG4gICAgcGFkZGluZzogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgLyogYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7ICovXHJcbiAgICAvKiAtd2Via2l0LWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuICAgIC1tb3otYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50OyAqL1xyXG4gIH1cclxuYFxyXG5leHBvcnQgY29uc3QgSG9yaXpvbnRhbFNwYWNlQmV0d2VlbiA9IHN0eWxlZC5kaXZgXHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5gXHJcblxyXG5leHBvcnQgY29uc3QgU21hbGxQYXBlciA9IHN0eWxlZC5kaXZgXHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIG1heC13aWR0aDogNDUwcHg7XHJcbiAgYmFja2dyb3VuZDogI2ZmZjtcclxuICBwYWRkaW5nOiAzMHB4O1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDEyMHB4O1xyXG4gIG1pbi1oZWlnaHQ6IDMwMHB4O1xyXG4gIGJveC1zaGFkb3c6IDAgMXB4IDJweCAwIHJnYmEoMzQsIDM2LCAzOCwgMC4xNSk7XHJcblxyXG4gIEBtZWRpYSAobWF4LXdpZHRoOiA3NjdweCkge1xyXG4gICAgbWFyZ2luLXRvcDogMzBweDtcclxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgLW1vei1ib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5gXHJcblxyXG5leHBvcnQgY29uc3QgRGVza3RvcE9ubHkgPSBzdHlsZWQuZGl2YFxyXG4gIEBtZWRpYSAobWF4LXdpZHRoOiA3NjdweCkge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICB9XHJcbmBcclxuXHJcbmV4cG9ydCBjb25zdCBNb2JpbGVPbmx5ID0gc3R5bGVkLmRpdmBcclxuICBkaXNwbGF5OiBub25lO1xyXG4gIEBtZWRpYSAobWF4LXdpZHRoOiA3NjdweCkge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICB9XHJcbmBcclxuXHJcbmV4cG9ydCBjb25zdCBTaWRlTWVudSA9IHN0eWxlZC5kaXZgXHJcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gIHBhZGRpbmc6IDE1cHg7XHJcbiAgZm9udC1zaXplOiAxcmVtO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuXHJcbiAgJHsocHJvcHMpID0+XHJcbiAgICBwcm9wcy5hY3RpdmUgJiZcclxuICAgIGNzc2BcclxuICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGB9XHJcbmBcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==