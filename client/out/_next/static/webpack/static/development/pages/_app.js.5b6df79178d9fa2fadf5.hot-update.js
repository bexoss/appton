webpackHotUpdate("static\\development\\pages\\_app.js",{

/***/ "./components/Layout/index.js":
/*!************************************!*\
  !*** ./components/Layout/index.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _context_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../context/auth */ "./context/auth.js");
/* harmony import */ var semantic_ui_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! semantic-ui-react */ "./node_modules/semantic-ui-react/dist/es/index.js");
/* harmony import */ var _Styles_Layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Styles/Layout */ "./components/Styles/Layout.js");
var _jsxFileName = "C:\\Users\\COM\\Projects\\appton\\client\\components\\Layout\\index.js",
    _s = $RefreshSig$();

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;







function Layout(_ref) {
  _s();

  var mobile = _ref.mobile,
      children = _ref.children;
  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"])();
  var context = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_context_auth__WEBPACK_IMPORTED_MODULE_3__["AuthContext"]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (context && router) {
      if (context.user) {
        if (router.pathname === '/login' || router.pathname === '/register') {
          router.push('/');
        }
      } else {
        if (router.pathname === '/profile') {
          router.push('/');
        }
      }
    }
  }, []);
  return __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Site"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 5
    }
  }, __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Header"], {
    isIndex: router.pathname === '/',
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }
  }, __jsx(semantic_ui_react__WEBPACK_IMPORTED_MODULE_4__["Container"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 9
    }
  }, __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["HeaderRow"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 11
    }
  }, __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["LogoWrapper"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 13
    }
  }, __jsx("span", {
    className: "kanit",
    style: {
      color: '#222'
    },
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 15
    }
  }, "Appton Inc."))))), __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Main"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 7
    }
  }, __jsx("div", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 9
    }
  }, children)), __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Footer"], {
    className: "sans",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 7
    }
  }, "Some footer!"));
}

_s(Layout, "YO1h8rPTZkc6xHy66nmNQ0d7s7s=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"]];
});

_c = Layout;
/* harmony default export */ __webpack_exports__["default"] = (Layout);

var _c;

$RefreshReg$(_c, "Layout");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports_1 = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports_1, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports_1)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports_1;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports_1)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL0xheW91dC9pbmRleC5qcyJdLCJuYW1lcyI6WyJMYXlvdXQiLCJtb2JpbGUiLCJjaGlsZHJlbiIsInJvdXRlciIsInVzZVJvdXRlciIsImNvbnRleHQiLCJ1c2VDb250ZXh0IiwiQXV0aENvbnRleHQiLCJ1c2VFZmZlY3QiLCJ1c2VyIiwicGF0aG5hbWUiLCJwdXNoIiwiY29sb3IiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTQSxNQUFULE9BQXNDO0FBQUE7O0FBQUEsTUFBcEJDLE1BQW9CLFFBQXBCQSxNQUFvQjtBQUFBLE1BQVpDLFFBQVksUUFBWkEsUUFBWTtBQUNwQyxNQUFNQyxNQUFNLEdBQUdDLDZEQUFTLEVBQXhCO0FBQ0EsTUFBTUMsT0FBTyxHQUFHQyx3REFBVSxDQUFDQyx5REFBRCxDQUExQjtBQUVBQyx5REFBUyxDQUFDLFlBQU07QUFDZCxRQUFJSCxPQUFPLElBQUlGLE1BQWYsRUFBdUI7QUFDckIsVUFBSUUsT0FBTyxDQUFDSSxJQUFaLEVBQWtCO0FBQ2hCLFlBQUlOLE1BQU0sQ0FBQ08sUUFBUCxLQUFvQixRQUFwQixJQUFnQ1AsTUFBTSxDQUFDTyxRQUFQLEtBQW9CLFdBQXhELEVBQXFFO0FBQ25FUCxnQkFBTSxDQUFDUSxJQUFQLENBQVksR0FBWjtBQUNEO0FBQ0YsT0FKRCxNQUlPO0FBQ0wsWUFBSVIsTUFBTSxDQUFDTyxRQUFQLEtBQW9CLFVBQXhCLEVBQW9DO0FBQ2xDUCxnQkFBTSxDQUFDUSxJQUFQLENBQVksR0FBWjtBQUNEO0FBQ0Y7QUFDRjtBQUNGLEdBWlEsRUFZTixFQVpNLENBQVQ7QUFjQSxTQUNFLE1BQUMsbURBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFLE1BQUMscURBQUQ7QUFBUSxXQUFPLEVBQUVSLE1BQU0sQ0FBQ08sUUFBUCxLQUFvQixHQUFyQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQywyREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQyx3REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQywwREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0U7QUFBTSxhQUFTLEVBQUMsT0FBaEI7QUFBd0IsU0FBSyxFQUFFO0FBQUVFLFdBQUssRUFBRTtBQUFULEtBQS9CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREYsQ0FERixDQURGLENBREYsQ0FERixFQWFFLE1BQUMsbURBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBTVYsUUFBTixDQURGLENBYkYsRUFpQkUsTUFBQyxxREFBRDtBQUFRLGFBQVMsRUFBQyxNQUFsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQWpCRixDQURGO0FBOENEOztHQWhFUUYsTTtVQUNRSSxxRDs7O0tBRFJKLE07QUFrRU1BLHFFQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3N0YXRpY1xcZGV2ZWxvcG1lbnRcXHBhZ2VzXFxfYXBwLmpzLjViNmRmNzkxNzhkOWZhMmZhZGY1LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgdXNlU3RhdGUsIHVzZUVmZmVjdCwgdXNlQ29udGV4dCB9IGZyb20gJ3JlYWN0J1xyXG5pbXBvcnQgTGluayBmcm9tICduZXh0L2xpbmsnXHJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gJ25leHQvcm91dGVyJ1xyXG5pbXBvcnQgeyBBdXRoQ29udGV4dCB9IGZyb20gJy4uLy4uL2NvbnRleHQvYXV0aCdcclxuaW1wb3J0IHsgSGVhZGVyIGFzIFNlbWFudGljSGVhZGVyLCBDb250YWluZXIsIEljb24sIE1lbnUsIFNpZGViYXIsIExpc3QsIFNlZ21lbnQsIEdyaWQgfSBmcm9tICdzZW1hbnRpYy11aS1yZWFjdCdcclxuaW1wb3J0IHsgU2l0ZSwgSGVhZGVyLCBMb2dvV3JhcHBlciwgSGVhZGVyUm93LCBNYWluLCBSZXNwb25zaXZlU2VwYXJhdG9yLCBSZXNwb25zaXZlQlIsIEZvb3RlciB9IGZyb20gJy4uL1N0eWxlcy9MYXlvdXQnXHJcblxyXG5mdW5jdGlvbiBMYXlvdXQoeyBtb2JpbGUsIGNoaWxkcmVuIH0pIHtcclxuICBjb25zdCByb3V0ZXIgPSB1c2VSb3V0ZXIoKVxyXG4gIGNvbnN0IGNvbnRleHQgPSB1c2VDb250ZXh0KEF1dGhDb250ZXh0KVxyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgaWYgKGNvbnRleHQgJiYgcm91dGVyKSB7XHJcbiAgICAgIGlmIChjb250ZXh0LnVzZXIpIHtcclxuICAgICAgICBpZiAocm91dGVyLnBhdGhuYW1lID09PSAnL2xvZ2luJyB8fCByb3V0ZXIucGF0aG5hbWUgPT09ICcvcmVnaXN0ZXInKSB7XHJcbiAgICAgICAgICByb3V0ZXIucHVzaCgnLycpXHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGlmIChyb3V0ZXIucGF0aG5hbWUgPT09ICcvcHJvZmlsZScpIHtcclxuICAgICAgICAgIHJvdXRlci5wdXNoKCcvJylcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9LCBbXSlcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxTaXRlPlxyXG4gICAgICA8SGVhZGVyIGlzSW5kZXg9e3JvdXRlci5wYXRobmFtZSA9PT0gJy8nfT5cclxuICAgICAgICA8Q29udGFpbmVyPlxyXG4gICAgICAgICAgPEhlYWRlclJvdz5cclxuICAgICAgICAgICAgPExvZ29XcmFwcGVyPlxyXG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT0na2FuaXQnIHN0eWxlPXt7IGNvbG9yOiAnIzIyMicgfX0+XHJcbiAgICAgICAgICAgICAgICBBcHB0b24gSW5jLlxyXG4gICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgPC9Mb2dvV3JhcHBlcj5cclxuICAgICAgICAgIDwvSGVhZGVyUm93PlxyXG4gICAgICAgIDwvQ29udGFpbmVyPlxyXG4gICAgICA8L0hlYWRlcj5cclxuXHJcbiAgICAgIDxNYWluPlxyXG4gICAgICAgIDxkaXY+e2NoaWxkcmVufTwvZGl2PlxyXG4gICAgICA8L01haW4+XHJcblxyXG4gICAgICA8Rm9vdGVyIGNsYXNzTmFtZT0nc2Fucyc+XHJcbiAgICAgICAgU29tZSBmb290ZXIhXHJcbiAgICAgICAgey8qIDxTZWdtZW50XHJcbiAgICAgICAgICB2ZXJ0aWNhbFxyXG4gICAgICAgICAgc3R5bGU9e3tcclxuICAgICAgICAgICAgcGFkZGluZzogJzFlbSAwZW0nLFxyXG4gICAgICAgICAgICBib3JkZXJUb3A6ICcxcHggc29saWQgI2VlZScsXHJcbiAgICAgICAgICAgIGZvbnRTaXplOiAnMC45cmVtJyxcclxuICAgICAgICAgIH19XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgPENvbnRhaW5lciB0ZXh0PlxyXG4gICAgICAgICAgICA8R3JpZCBkaXZpZGVkIHN0YWNrYWJsZT5cclxuICAgICAgICAgICAgICA8R3JpZC5Sb3c+XHJcbiAgICAgICAgICAgICAgICA8R3JpZC5Db2x1bW4gd2lkdGg9ezE2fT5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17eyBjb2xvcjogJyNhYWEnLCBsaW5lSGVpZ2h0OiAnMS44cmVtJywgZm9udFNpemU6ICcxcmVtJyB9fSBjbGFzc05hbWU9J3NhbnMnPlxyXG4gICAgICAgICAgICAgICAgICAgICjso7wp7JWx7YakIDxSZXNwb25zaXZlU2VwYXJhdG9yPnw8L1Jlc3BvbnNpdmVTZXBhcmF0b3I+IOyEnOyauOyLnCDsooXroZzqtawg7KKF66GcIDUxIOyiheuhnO2DgOybjCAxOOy4tVxyXG4gICAgICAgICAgICAgICAgICAgIDxiciAvPlxyXG4gICAgICAgICAgICAgICAgICAgIOyCrOyXheyekOuTseuhneuyiO2YuCA4OTctODctMDE1MTYgPFJlc3BvbnNpdmVTZXBhcmF0b3I+fDwvUmVzcG9uc2l2ZVNlcGFyYXRvcj7rjIDtkZzsnpAg6rmA7KSA7YOcIDxSZXNwb25zaXZlU2VwYXJhdG9yPnw8L1Jlc3BvbnNpdmVTZXBhcmF0b3I+XHJcbiAgICAgICAgICAgICAgICAgICAgPFJlc3BvbnNpdmVCUiAvPiDsoITtmZQgMTU0NC0zMTk3IHwg7J2066mU7J28IGNzQGFwcHRvbi5jby5rclxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvR3JpZC5Db2x1bW4+XHJcbiAgICAgICAgICAgICAgPC9HcmlkLlJvdz5cclxuICAgICAgICAgICAgPC9HcmlkPlxyXG4gICAgICAgICAgPC9Db250YWluZXI+XHJcbiAgICAgICAgPC9TZWdtZW50PiAqL31cclxuICAgICAgPC9Gb290ZXI+XHJcbiAgICA8L1NpdGU+XHJcbiAgKVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBMYXlvdXRcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==