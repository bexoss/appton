webpackHotUpdate("static\\development\\pages\\_app.js",{

/***/ "./components/Layout/index.js":
/*!************************************!*\
  !*** ./components/Layout/index.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _context_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../context/auth */ "./context/auth.js");
/* harmony import */ var semantic_ui_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! semantic-ui-react */ "./node_modules/semantic-ui-react/dist/es/index.js");
/* harmony import */ var _Styles_Layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Styles/Layout */ "./components/Styles/Layout.js");
var _jsxFileName = "C:\\Users\\COM\\Projects\\appton\\client\\components\\Layout\\index.js",
    _s = $RefreshSig$();

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;







function Layout(_ref) {
  _s();

  var mobile = _ref.mobile,
      children = _ref.children;
  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"])();
  var context = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_context_auth__WEBPACK_IMPORTED_MODULE_3__["AuthContext"]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (context && router) {
      if (context.user) {
        if (router.pathname === '/login' || router.pathname === '/register') {
          router.push('/');
        }
      } else {
        if (router.pathname === '/profile') {
          router.push('/');
        }
      }
    }
  }, []);
  return __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Site"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 5
    }
  }, __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Header"], {
    isIndex: router.pathname === '/',
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }
  }, __jsx(semantic_ui_react__WEBPACK_IMPORTED_MODULE_4__["Container"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 9
    }
  }, __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["HeaderRow"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 11
    }
  }, __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["LogoWrapper"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 13
    }
  }, __jsx("span", {
    className: "kanit",
    style: {
      color: '#222'
    },
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 15
    }
  }, "Appton Inc."))))), __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Main"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 7
    }
  }, __jsx("div", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 9
    }
  }, children)), __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Footer"], {
    className: "sans",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 7
    }
  }, "Some footer!"));
}

_s(Layout, "YO1h8rPTZkc6xHy66nmNQ0d7s7s=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"]];
});

_c = Layout;
/* harmony default export */ __webpack_exports__["default"] = (Layout);

var _c;

$RefreshReg$(_c, "Layout");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports_1 = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports_1, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports_1)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports_1;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports_1)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL0xheW91dC9pbmRleC5qcyJdLCJuYW1lcyI6WyJMYXlvdXQiLCJtb2JpbGUiLCJjaGlsZHJlbiIsInJvdXRlciIsInVzZVJvdXRlciIsImNvbnRleHQiLCJ1c2VDb250ZXh0IiwiQXV0aENvbnRleHQiLCJ1c2VFZmZlY3QiLCJ1c2VyIiwicGF0aG5hbWUiLCJwdXNoIiwiY29sb3IiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTQSxNQUFULE9BQXNDO0FBQUE7O0FBQUEsTUFBcEJDLE1BQW9CLFFBQXBCQSxNQUFvQjtBQUFBLE1BQVpDLFFBQVksUUFBWkEsUUFBWTtBQUNwQyxNQUFNQyxNQUFNLEdBQUdDLDZEQUFTLEVBQXhCO0FBQ0EsTUFBTUMsT0FBTyxHQUFHQyx3REFBVSxDQUFDQyx5REFBRCxDQUExQjtBQUVBQyx5REFBUyxDQUFDLFlBQU07QUFDZCxRQUFJSCxPQUFPLElBQUlGLE1BQWYsRUFBdUI7QUFDckIsVUFBSUUsT0FBTyxDQUFDSSxJQUFaLEVBQWtCO0FBQ2hCLFlBQUlOLE1BQU0sQ0FBQ08sUUFBUCxLQUFvQixRQUFwQixJQUFnQ1AsTUFBTSxDQUFDTyxRQUFQLEtBQW9CLFdBQXhELEVBQXFFO0FBQ25FUCxnQkFBTSxDQUFDUSxJQUFQLENBQVksR0FBWjtBQUNEO0FBQ0YsT0FKRCxNQUlPO0FBQ0wsWUFBSVIsTUFBTSxDQUFDTyxRQUFQLEtBQW9CLFVBQXhCLEVBQW9DO0FBQ2xDUCxnQkFBTSxDQUFDUSxJQUFQLENBQVksR0FBWjtBQUNEO0FBQ0Y7QUFDRjtBQUNGLEdBWlEsRUFZTixFQVpNLENBQVQ7QUFjQSxTQUNFLE1BQUMsbURBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFLE1BQUMscURBQUQ7QUFBUSxXQUFPLEVBQUVSLE1BQU0sQ0FBQ08sUUFBUCxLQUFvQixHQUFyQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQywyREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQyx3REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQywwREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0U7QUFBTSxhQUFTLEVBQUMsT0FBaEI7QUFBd0IsU0FBSyxFQUFFO0FBQUVFLFdBQUssRUFBRTtBQUFULEtBQS9CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREYsQ0FERixDQURGLENBREYsQ0FERixFQWFFLE1BQUMsbURBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBTVYsUUFBTixDQURGLENBYkYsRUFpQkUsTUFBQyxxREFBRDtBQUFRLGFBQVMsRUFBQyxNQUFsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQWpCRixDQURGO0FBOENEOztHQWhFUUYsTTtVQUNRSSxxRDs7O0tBRFJKLE07QUFrRU1BLHFFQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3N0YXRpY1xcZGV2ZWxvcG1lbnRcXHBhZ2VzXFxfYXBwLmpzLjFmNGRhZmE4ZWYyODE3M2UyYWE4LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgdXNlU3RhdGUsIHVzZUVmZmVjdCwgdXNlQ29udGV4dCB9IGZyb20gJ3JlYWN0J1xyXG5pbXBvcnQgTGluayBmcm9tICduZXh0L2xpbmsnXHJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gJ25leHQvcm91dGVyJ1xyXG5pbXBvcnQgeyBBdXRoQ29udGV4dCB9IGZyb20gJy4uLy4uL2NvbnRleHQvYXV0aCdcclxuaW1wb3J0IHsgSGVhZGVyIGFzIFNlbWFudGljSGVhZGVyLCBDb250YWluZXIsIH0gZnJvbSAnc2VtYW50aWMtdWktcmVhY3QnXHJcbmltcG9ydCB7IFNpdGUsIEhlYWRlciwgTG9nb1dyYXBwZXIsIEhlYWRlclJvdywgTWFpbiwgRm9vdGVyIH0gZnJvbSAnLi4vU3R5bGVzL0xheW91dCdcclxuXHJcbmZ1bmN0aW9uIExheW91dCh7IG1vYmlsZSwgY2hpbGRyZW4gfSkge1xyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpXHJcbiAgY29uc3QgY29udGV4dCA9IHVzZUNvbnRleHQoQXV0aENvbnRleHQpXHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAoY29udGV4dCAmJiByb3V0ZXIpIHtcclxuICAgICAgaWYgKGNvbnRleHQudXNlcikge1xyXG4gICAgICAgIGlmIChyb3V0ZXIucGF0aG5hbWUgPT09ICcvbG9naW4nIHx8IHJvdXRlci5wYXRobmFtZSA9PT0gJy9yZWdpc3RlcicpIHtcclxuICAgICAgICAgIHJvdXRlci5wdXNoKCcvJylcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKHJvdXRlci5wYXRobmFtZSA9PT0gJy9wcm9maWxlJykge1xyXG4gICAgICAgICAgcm91dGVyLnB1c2goJy8nKVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH0sIFtdKVxyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPFNpdGU+XHJcbiAgICAgIDxIZWFkZXIgaXNJbmRleD17cm91dGVyLnBhdGhuYW1lID09PSAnLyd9PlxyXG4gICAgICAgIDxDb250YWluZXI+XHJcbiAgICAgICAgICA8SGVhZGVyUm93PlxyXG4gICAgICAgICAgICA8TG9nb1dyYXBwZXI+XHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPSdrYW5pdCcgc3R5bGU9e3sgY29sb3I6ICcjMjIyJyB9fT5cclxuICAgICAgICAgICAgICAgIEFwcHRvbiBJbmMuXHJcbiAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICA8L0xvZ29XcmFwcGVyPlxyXG4gICAgICAgICAgPC9IZWFkZXJSb3c+XHJcbiAgICAgICAgPC9Db250YWluZXI+XHJcbiAgICAgIDwvSGVhZGVyPlxyXG5cclxuICAgICAgPE1haW4+XHJcbiAgICAgICAgPGRpdj57Y2hpbGRyZW59PC9kaXY+XHJcbiAgICAgIDwvTWFpbj5cclxuXHJcbiAgICAgIDxGb290ZXIgY2xhc3NOYW1lPSdzYW5zJz5cclxuICAgICAgICBTb21lIGZvb3RlciFcclxuICAgICAgICB7LyogPFNlZ21lbnRcclxuICAgICAgICAgIHZlcnRpY2FsXHJcbiAgICAgICAgICBzdHlsZT17e1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAnMWVtIDBlbScsXHJcbiAgICAgICAgICAgIGJvcmRlclRvcDogJzFweCBzb2xpZCAjZWVlJyxcclxuICAgICAgICAgICAgZm9udFNpemU6ICcwLjlyZW0nLFxyXG4gICAgICAgICAgfX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICA8Q29udGFpbmVyIHRleHQ+XHJcbiAgICAgICAgICAgIDxHcmlkIGRpdmlkZWQgc3RhY2thYmxlPlxyXG4gICAgICAgICAgICAgIDxHcmlkLlJvdz5cclxuICAgICAgICAgICAgICAgIDxHcmlkLkNvbHVtbiB3aWR0aD17MTZ9PlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7IGNvbG9yOiAnI2FhYScsIGxpbmVIZWlnaHQ6ICcxLjhyZW0nLCBmb250U2l6ZTogJzFyZW0nIH19IGNsYXNzTmFtZT0nc2Fucyc+XHJcbiAgICAgICAgICAgICAgICAgICAgKOyjvCnslbHthqQgPFJlc3BvbnNpdmVTZXBhcmF0b3I+fDwvUmVzcG9uc2l2ZVNlcGFyYXRvcj4g7ISc7Jq47IucIOyiheuhnOq1rCDsooXroZwgNTEg7KKF66Gc7YOA7JuMIDE47Li1XHJcbiAgICAgICAgICAgICAgICAgICAgPGJyIC8+XHJcbiAgICAgICAgICAgICAgICAgICAg7IKs7JeF7J6Q65Ox66Gd67KI7Zi4IDg5Ny04Ny0wMTUxNiA8UmVzcG9uc2l2ZVNlcGFyYXRvcj58PC9SZXNwb25zaXZlU2VwYXJhdG9yPuuMgO2RnOyekCDquYDspIDtg5wgPFJlc3BvbnNpdmVTZXBhcmF0b3I+fDwvUmVzcG9uc2l2ZVNlcGFyYXRvcj5cclxuICAgICAgICAgICAgICAgICAgICA8UmVzcG9uc2l2ZUJSIC8+IOyghO2ZlCAxNTQ0LTMxOTcgfCDsnbTrqZTsnbwgY3NAYXBwdG9uLmNvLmtyXHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9HcmlkLkNvbHVtbj5cclxuICAgICAgICAgICAgICA8L0dyaWQuUm93PlxyXG4gICAgICAgICAgICA8L0dyaWQ+XHJcbiAgICAgICAgICA8L0NvbnRhaW5lcj5cclxuICAgICAgICA8L1NlZ21lbnQ+ICovfVxyXG4gICAgICA8L0Zvb3Rlcj5cclxuICAgIDwvU2l0ZT5cclxuICApXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IExheW91dFxyXG4iXSwic291cmNlUm9vdCI6IiJ9