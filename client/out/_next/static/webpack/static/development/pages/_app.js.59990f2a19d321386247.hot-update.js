webpackHotUpdate("static\\development\\pages\\_app.js",{

/***/ "./components/Layout/index.js":
/*!************************************!*\
  !*** ./components/Layout/index.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _context_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../context/auth */ "./context/auth.js");
/* harmony import */ var semantic_ui_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! semantic-ui-react */ "./node_modules/semantic-ui-react/dist/es/index.js");
/* harmony import */ var _Styles_Layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Styles/Layout */ "./components/Styles/Layout.js");
var _jsxFileName = "C:\\Users\\COM\\Projects\\appton\\client\\components\\Layout\\index.js",
    _s = $RefreshSig$();

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;







function Layout(_ref) {
  _s();

  var mobile = _ref.mobile,
      children = _ref.children;
  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"])();
  var context = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_context_auth__WEBPACK_IMPORTED_MODULE_3__["AuthContext"]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (context && router) {
      if (context.user) {
        if (router.pathname === '/login' || router.pathname === '/register') {
          router.push('/');
        }
      } else {
        if (router.pathname === '/profile') {
          router.push('/');
        }
      }
    }
  }, []);
  return __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Site"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 5
    }
  }, __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Header"], {
    isIndex: router.pathname === '/',
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }
  }, __jsx(semantic_ui_react__WEBPACK_IMPORTED_MODULE_4__["Container"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 9
    }
  }, __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["HeaderRow"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 11
    }
  }, __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["LogoWrapper"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 13
    }
  }, __jsx("span", {
    className: "kanit",
    style: {
      color: '#222'
    },
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 15
    }
  }, "Appton Inc."))))), __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Main"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 7
    }
  }, __jsx("div", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 9
    }
  }, children)), __jsx(_Styles_Layout__WEBPACK_IMPORTED_MODULE_5__["Footer"], {
    className: "sans",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 7
    }
  }, __jsx(Segment, {
    vertical: true,
    style: {
      padding: '1em 0em',
      borderTop: '1px solid #eee',
      fontSize: '0.9rem'
    },
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 9
    }
  }, __jsx(semantic_ui_react__WEBPACK_IMPORTED_MODULE_4__["Container"], {
    text: true,
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 11
    }
  }, __jsx(Grid, {
    divided: true,
    stackable: true,
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54,
      columnNumber: 13
    }
  }, __jsx(Grid.Row, {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 15
    }
  }, __jsx(Grid.Column, {
    width: 16,
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 17
    }
  }, __jsx("div", {
    style: {
      color: '#aaa',
      lineHeight: '1.8rem',
      fontSize: '1rem'
    },
    className: "sans",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 19
    }
  }, "(\uC8FC)\uC571\uD1A4 ", __jsx(ResponsiveSeparator, {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 27
    }
  }, "|"), " \uC11C\uC6B8\uC2DC \uC885\uB85C\uAD6C \uC885\uB85C 51 \uC885\uB85C\uD0C0\uC6CC 18\uCE35", __jsx("br", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59,
      columnNumber: 21
    }
  }), "\uC0AC\uC5C5\uC790\uB4F1\uB85D\uBC88\uD638 897-87-01516 ", __jsx(ResponsiveSeparator, {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60,
      columnNumber: 42
    }
  }, "|"), "\uB300\uD45C\uC790 \uAE40\uC900\uD0DC ", __jsx(ResponsiveSeparator, {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60,
      columnNumber: 94
    }
  }, "|"), __jsx(ResponsiveBR, {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61,
      columnNumber: 21
    }
  }), " \uC804\uD654 1544-3197 | \uC774\uBA54\uC77C cs@appton.co.kr"))))))));
}

_s(Layout, "YO1h8rPTZkc6xHy66nmNQ0d7s7s=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"]];
});

_c = Layout;
/* harmony default export */ __webpack_exports__["default"] = (Layout);

var _c;

$RefreshReg$(_c, "Layout");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports_1 = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports_1, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports_1)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports_1;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports_1)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL0xheW91dC9pbmRleC5qcyJdLCJuYW1lcyI6WyJMYXlvdXQiLCJtb2JpbGUiLCJjaGlsZHJlbiIsInJvdXRlciIsInVzZVJvdXRlciIsImNvbnRleHQiLCJ1c2VDb250ZXh0IiwiQXV0aENvbnRleHQiLCJ1c2VFZmZlY3QiLCJ1c2VyIiwicGF0aG5hbWUiLCJwdXNoIiwiY29sb3IiLCJwYWRkaW5nIiwiYm9yZGVyVG9wIiwiZm9udFNpemUiLCJsaW5lSGVpZ2h0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsU0FBU0EsTUFBVCxPQUFzQztBQUFBOztBQUFBLE1BQXBCQyxNQUFvQixRQUFwQkEsTUFBb0I7QUFBQSxNQUFaQyxRQUFZLFFBQVpBLFFBQVk7QUFDcEMsTUFBTUMsTUFBTSxHQUFHQyw2REFBUyxFQUF4QjtBQUNBLE1BQU1DLE9BQU8sR0FBR0Msd0RBQVUsQ0FBQ0MseURBQUQsQ0FBMUI7QUFFQUMseURBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSUgsT0FBTyxJQUFJRixNQUFmLEVBQXVCO0FBQ3JCLFVBQUlFLE9BQU8sQ0FBQ0ksSUFBWixFQUFrQjtBQUNoQixZQUFJTixNQUFNLENBQUNPLFFBQVAsS0FBb0IsUUFBcEIsSUFBZ0NQLE1BQU0sQ0FBQ08sUUFBUCxLQUFvQixXQUF4RCxFQUFxRTtBQUNuRVAsZ0JBQU0sQ0FBQ1EsSUFBUCxDQUFZLEdBQVo7QUFDRDtBQUNGLE9BSkQsTUFJTztBQUNMLFlBQUlSLE1BQU0sQ0FBQ08sUUFBUCxLQUFvQixVQUF4QixFQUFvQztBQUNsQ1AsZ0JBQU0sQ0FBQ1EsSUFBUCxDQUFZLEdBQVo7QUFDRDtBQUNGO0FBQ0Y7QUFDRixHQVpRLEVBWU4sRUFaTSxDQUFUO0FBY0EsU0FDRSxNQUFDLG1EQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDRSxNQUFDLHFEQUFEO0FBQVEsV0FBTyxFQUFFUixNQUFNLENBQUNPLFFBQVAsS0FBb0IsR0FBckM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFLE1BQUMsMkRBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFLE1BQUMsd0RBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFLE1BQUMsMERBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFO0FBQU0sYUFBUyxFQUFDLE9BQWhCO0FBQXdCLFNBQUssRUFBRTtBQUFFRSxXQUFLLEVBQUU7QUFBVCxLQUEvQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURGLENBREYsQ0FERixDQURGLENBREYsRUFhRSxNQUFDLG1EQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQU1WLFFBQU4sQ0FERixDQWJGLEVBaUJFLE1BQUMscURBQUQ7QUFBUSxhQUFTLEVBQUMsTUFBbEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUNFLE1BQUMsT0FBRDtBQUNFLFlBQVEsTUFEVjtBQUVFLFNBQUssRUFBRTtBQUNMVyxhQUFPLEVBQUUsU0FESjtBQUVMQyxlQUFTLEVBQUUsZ0JBRk47QUFHTEMsY0FBUSxFQUFFO0FBSEwsS0FGVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBUUUsTUFBQywyREFBRDtBQUFXLFFBQUksTUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQyxJQUFEO0FBQU0sV0FBTyxNQUFiO0FBQWMsYUFBUyxNQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQyxJQUFELENBQU0sR0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0UsTUFBQyxJQUFELENBQU0sTUFBTjtBQUFhLFNBQUssRUFBRSxFQUFwQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQ0U7QUFBSyxTQUFLLEVBQUU7QUFBRUgsV0FBSyxFQUFFLE1BQVQ7QUFBaUJJLGdCQUFVLEVBQUUsUUFBN0I7QUFBdUNELGNBQVEsRUFBRTtBQUFqRCxLQUFaO0FBQXVFLGFBQVMsRUFBQyxNQUFqRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDhCQUNRLE1BQUMsbUJBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQURSLDhGQUVFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsSUFGRiw4REFHdUIsTUFBQyxtQkFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBSHZCLDRDQUcyRSxNQUFDLG1CQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FIM0UsRUFJRSxNQUFDLFlBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxJQUpGLGlFQURGLENBREYsQ0FERixDQURGLENBUkYsQ0FERixDQWpCRixDQURGO0FBNkNEOztHQS9EUWYsTTtVQUNRSSxxRDs7O0tBRFJKLE07QUFpRU1BLHFFQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3N0YXRpY1xcZGV2ZWxvcG1lbnRcXHBhZ2VzXFxfYXBwLmpzLjU5OTkwZjJhMTlkMzIxMzg2MjQ3LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgdXNlU3RhdGUsIHVzZUVmZmVjdCwgdXNlQ29udGV4dCB9IGZyb20gJ3JlYWN0J1xyXG5pbXBvcnQgTGluayBmcm9tICduZXh0L2xpbmsnXHJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gJ25leHQvcm91dGVyJ1xyXG5pbXBvcnQgeyBBdXRoQ29udGV4dCB9IGZyb20gJy4uLy4uL2NvbnRleHQvYXV0aCdcclxuaW1wb3J0IHsgQ29udGFpbmVyIH0gZnJvbSAnc2VtYW50aWMtdWktcmVhY3QnXHJcbmltcG9ydCB7IFNpdGUsIEhlYWRlciwgTG9nb1dyYXBwZXIsIEhlYWRlclJvdywgTWFpbiwgRm9vdGVyIH0gZnJvbSAnLi4vU3R5bGVzL0xheW91dCdcclxuXHJcbmZ1bmN0aW9uIExheW91dCh7IG1vYmlsZSwgY2hpbGRyZW4gfSkge1xyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpXHJcbiAgY29uc3QgY29udGV4dCA9IHVzZUNvbnRleHQoQXV0aENvbnRleHQpXHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAoY29udGV4dCAmJiByb3V0ZXIpIHtcclxuICAgICAgaWYgKGNvbnRleHQudXNlcikge1xyXG4gICAgICAgIGlmIChyb3V0ZXIucGF0aG5hbWUgPT09ICcvbG9naW4nIHx8IHJvdXRlci5wYXRobmFtZSA9PT0gJy9yZWdpc3RlcicpIHtcclxuICAgICAgICAgIHJvdXRlci5wdXNoKCcvJylcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKHJvdXRlci5wYXRobmFtZSA9PT0gJy9wcm9maWxlJykge1xyXG4gICAgICAgICAgcm91dGVyLnB1c2goJy8nKVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH0sIFtdKVxyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPFNpdGU+XHJcbiAgICAgIDxIZWFkZXIgaXNJbmRleD17cm91dGVyLnBhdGhuYW1lID09PSAnLyd9PlxyXG4gICAgICAgIDxDb250YWluZXI+XHJcbiAgICAgICAgICA8SGVhZGVyUm93PlxyXG4gICAgICAgICAgICA8TG9nb1dyYXBwZXI+XHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPSdrYW5pdCcgc3R5bGU9e3sgY29sb3I6ICcjMjIyJyB9fT5cclxuICAgICAgICAgICAgICAgIEFwcHRvbiBJbmMuXHJcbiAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICA8L0xvZ29XcmFwcGVyPlxyXG4gICAgICAgICAgPC9IZWFkZXJSb3c+XHJcbiAgICAgICAgPC9Db250YWluZXI+XHJcbiAgICAgIDwvSGVhZGVyPlxyXG5cclxuICAgICAgPE1haW4+XHJcbiAgICAgICAgPGRpdj57Y2hpbGRyZW59PC9kaXY+XHJcbiAgICAgIDwvTWFpbj5cclxuXHJcbiAgICAgIDxGb290ZXIgY2xhc3NOYW1lPSdzYW5zJz5cclxuICAgICAgICA8U2VnbWVudFxyXG4gICAgICAgICAgdmVydGljYWxcclxuICAgICAgICAgIHN0eWxlPXt7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6ICcxZW0gMGVtJyxcclxuICAgICAgICAgICAgYm9yZGVyVG9wOiAnMXB4IHNvbGlkICNlZWUnLFxyXG4gICAgICAgICAgICBmb250U2l6ZTogJzAuOXJlbScsXHJcbiAgICAgICAgICB9fVxyXG4gICAgICAgID5cclxuICAgICAgICAgIDxDb250YWluZXIgdGV4dD5cclxuICAgICAgICAgICAgPEdyaWQgZGl2aWRlZCBzdGFja2FibGU+XHJcbiAgICAgICAgICAgICAgPEdyaWQuUm93PlxyXG4gICAgICAgICAgICAgICAgPEdyaWQuQ29sdW1uIHdpZHRoPXsxNn0+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3sgY29sb3I6ICcjYWFhJywgbGluZUhlaWdodDogJzEuOHJlbScsIGZvbnRTaXplOiAnMXJlbScgfX0gY2xhc3NOYW1lPSdzYW5zJz5cclxuICAgICAgICAgICAgICAgICAgICAo7KO8KeyVse2GpCA8UmVzcG9uc2l2ZVNlcGFyYXRvcj58PC9SZXNwb25zaXZlU2VwYXJhdG9yPiDshJzsmrjsi5wg7KKF66Gc6rWsIOyiheuhnCA1MSDsooXroZztg4Dsm4wgMTjsuLVcclxuICAgICAgICAgICAgICAgICAgICA8YnIgLz5cclxuICAgICAgICAgICAgICAgICAgICDsgqzsl4XsnpDrk7HroZ3rsojtmLggODk3LTg3LTAxNTE2IDxSZXNwb25zaXZlU2VwYXJhdG9yPnw8L1Jlc3BvbnNpdmVTZXBhcmF0b3I+64yA7ZGc7J6QIOq5gOykgO2DnCA8UmVzcG9uc2l2ZVNlcGFyYXRvcj58PC9SZXNwb25zaXZlU2VwYXJhdG9yPlxyXG4gICAgICAgICAgICAgICAgICAgIDxSZXNwb25zaXZlQlIgLz4g7KCE7ZmUIDE1NDQtMzE5NyB8IOydtOuplOydvCBjc0BhcHB0b24uY28ua3JcclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L0dyaWQuQ29sdW1uPlxyXG4gICAgICAgICAgICAgIDwvR3JpZC5Sb3c+XHJcbiAgICAgICAgICAgIDwvR3JpZD5cclxuICAgICAgICAgIDwvQ29udGFpbmVyPlxyXG4gICAgICAgIDwvU2VnbWVudD5cclxuICAgICAgPC9Gb290ZXI+XHJcbiAgICA8L1NpdGU+XHJcbiAgKVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBMYXlvdXRcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==