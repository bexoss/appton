import { useState, useEffect, useContext } from 'react'
import { AuthContext } from '../../context/auth'
import { useRouter } from 'next/router'
import List from './List'
import Read from './Read'
import Write from './Write'
import Update from './Update'
import { PaperTitle } from '../Styles/Layout'
import Link from 'next/link'
import ClientOnly from '../ClientOnly'

function Board(props) {
  const router = useRouter()
  const { user } = useContext(AuthContext)
  
  const boardProps = { postId: router.query.postId, ...props }
  const getMode = () => {
    if (router.query.mode === 'update') return 'update'
    if (router.query.postId && !router.query.mode) return 'read'
    if (router.query.mode === 'write') return 'write'
    if (!router.query.postId && !router.pathname.includes('/write')) return 'list'
  }

  const mode = getMode()

  return (
    <div>
      <div className='boardHeader'>
        <div>
          <PaperTitle className="handon">{props.title}</PaperTitle>
        </div>
        {mode === 'list' && (
          <ClientOnly>
            {user  && (
              <Link href={`${props.baseUrl}?mode=write`}>
                <div className='textButton handon'>글쓰기</div>
              </Link>
            )}
          </ClientOnly>
        )}
        {mode === 'update' && (
          <Link href={`${props.baseUrl}?postId=${router.query.postId}`}>
            <div className='textButton handon'>취소</div>
          </Link>
        )}
      </div>
      {mode === 'update' && <Update {...boardProps} />}
      {mode === 'read' && <Read {...boardProps} />}
      {mode === 'write' && <Write {...boardProps} />}
      {mode === 'list' && <List {...boardProps} />}
    </div>
  )
}

export default Board