import { useEffect, useState } from 'react'
import { useQuery } from '@apollo/react-hooks'
import Router, { useRouter } from 'next/router'
import { GET_POSTS } from '../../util/gql'
import { useCount } from '../../util/hooks'
import { Table, Message, Pagination } from 'semantic-ui-react'
import Link from 'next/link'
import { withApollo } from '../../lib/withApollo'
import moment from 'moment'
import('moment/locale/ko')
moment.locale('ko')

function List({ collection, mid = null, baseUrl, current = null }) {
  const router = useRouter()
  const [perPage, setPerPage] = useState(5)

  const { loading, data, error, fetchMore } = useQuery(GET_POSTS, {
    variables: {
      mid,
      perPage,
      page: router.query.page ? Number(router.query.page) : 1,
    },
    fetchPolicy: 'cache-and-network',
  })

  const { loadCount, loading: totalCountLoading, data: totalCount } = useCount({
    collection,
    mid,
  })

  useEffect(() => {
    loadCount()
  }, [router.query.search])

  if (error) {
    return (
      <Message error>
        <Message.Header>Error</Message.Header>
        <pre>{JSON.stringify(error, null, 4)}</pre>
        <p>{error.graphQLErrors[0] && error.graphQLErrors[0].message}</p>
      </Message>
    )
  }

  if (loading) {
    return <div>loading...</div>
  }

  return (
    <div style={{ paddingBottom: 50 }}>
      <style jsx>{`
        table td {
          border: 1px solid red;
        }
      `}</style>
      <Table
        selectable
        unstackable
        style={{
          border: 'none',
          borderTop: '1px solid #DFE0E5',
          borderBottom: '1px solid #DFE0E5',
          borderRadius: 0,
        }}
      >
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width={10}>제목</Table.HeaderCell>
            <Table.HeaderCell width={2} textAlign='center' style={{ whiteSpace: 'nowrap' }}>
              시각
            </Table.HeaderCell>
            <Table.HeaderCell width={2} textAlign='center' style={{ whiteSpace: 'nowrap' }}>
              글쓴이
            </Table.HeaderCell>
            <Table.HeaderCell width={2} textAlign='center' style={{ whiteSpace: 'nowrap' }}>
              조회
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {!loading &&
            data &&
            data.getPosts.map((row) => {
              return (
                <Table.Row
                  key={row.id}
                  style={{ background: !current && row.isNotice ? '#ceebfd' : 'transparent', fontWeight: !current && row.isNotice ? '600' : '400' }}
                >
                  <Table.Cell
                    style={{
                      whiteSpace: 'nowrap',
                      overflow: 'hidden',
                      textOverflow: 'ellipsis',
                      maxWidth: 20,
                    }}
                  >
                    <Link
                      href={{
                        pathname: baseUrl,
                        query: Object.assign({}, { ...router.query }, { postId: row.id }),
                      }}
                    >
                      {/* // href={`${baseUrl}?postId=${row.id}`}> */}
                      <a>
                        {current && row.id === current ? '>> ' : ''}{row.title}{' '}
                        {row.comments.length > 0 && <strong>[{row.comments.length}]</strong>}
                      </a>
                    </Link>
                  </Table.Cell>
                  <Table.Cell
                    textAlign='center'
                    style={{
                      whiteSpace: 'nowrap',
                      overflow: 'hidden',
                      textOverflow: 'ellipsis',
                      maxWidth: 100,
                    }}
                  >
                    {moment.unix(row.createdAt).fromNow()}
                  </Table.Cell>
                  <Table.Cell textAlign='center' style={{ whiteSpace: 'nowrap' }}>
                    {row.user.name}
                  </Table.Cell>
                  <Table.Cell textAlign="center" style={{ whiteSpace: 'nowrap' }}>
                    {row.viewCount}
                  </Table.Cell>
                </Table.Row>
              )
            })}
        </Table.Body>
      </Table>

      <div style={{ display: 'flex', justifyContent: 'center' }}>
        {!totalCountLoading && totalCount && (
          <Pagination
            size='mini'
            defaultActivePage={router.query.page ? router.query.page : 1}
            totalPages={Math.ceil(totalCount.getCount / perPage)}
            onPageChange={(e, data) => {
              Router.push({
                pathname: baseUrl,
                query: Object.assign({}, { ...router.query }, { page: data.activePage }),
              })
            }}
          />
        )}
      </div>
    </div>
  )
}

export default List
