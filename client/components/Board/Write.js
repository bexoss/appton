import React, { useState } from 'react'
import { useMutation } from '@apollo/react-hooks'
import Editor from '../../components/Editor'
import { useRouter } from 'next/router'
import { ADD_POST } from '../../util/gql'
import { GET_DOCUMENTS } from '../../util/gql-admin'
import { Input, Button } from 'semantic-ui-react'

function Write({ mid, baseUrl }) {
  const router = useRouter()
  const [title, setTitle] = useState('')
  const [content, setContent] = useState('')

  const cacheQuery = {
    query: GET_DOCUMENTS,
    variables: {
      collection: 'posts',
      mid,
      page: router.query.page ? Number(router.query.page) : 1,
      search: router.query.search ? router.query.search : null,
    },
  }

  const [addPost, { loading, error }] = useMutation(ADD_POST, {
    update(cache, { data: { addPost } }) {
      router.push(baseUrl)
      // TODO: 문제 해결 해야함.
      // const data = cache.readQuery({
      //   query: GET_DOCUMENTS,
      //   variables: {
      //     collection: 'posts',
      //     mid
      //   },
      // })
      // data.getDocuments = [addPost, ...data.getDocuments]
      // cache.writeQuery(Object.assign({}, cacheQuery, data))
    },
    variables: { title, content, mid },
    onError(e) {
      console.log(JSON.stringify(e))
    }
  })

  const handleChange = (value) => {
    setContent(value)
  }

  return (
    <div>
      <Input
        fluid
        value={title}
        placeholder={'이곳에 제목을 입력하세요.'}
        onChange={(e) => setTitle(e.target.value)}
        style={{ marginBottom: 20 }}
      />
      <div style={{ marginBottom: 20 }}>
        <Editor handleChange={handleChange} content={content} />
      </div>

      <Button primary fluid onClick={addPost}>
        등록
      </Button>
    </div>
  )
}

export default Write