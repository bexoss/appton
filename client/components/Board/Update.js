import React, { useState } from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import Editor from '../../components/Editor'
import { useRouter } from 'next/router'
import { GET_SINGLE_POST, UPDATE_POST } from '../../util/gql'
import { Input, Button } from 'semantic-ui-react'
import Error from 'next/error'

function Update({ postId, baseUrl }) {
  const router = useRouter()
  const [title, setTitle] = useState('')
  const [content, setContent] = useState('')

  const { loading, data, error } = useQuery(GET_SINGLE_POST, {
    variables: { id: postId },
    onCompleted(data) {
      setTitle(data.getSinglePost.title)
      setContent(data.getSinglePost.content)
    },
  })

  const [updatePost] = useMutation(UPDATE_POST, {
    update(cache, result) {
      router.push(baseUrl)
    },
    onError(err) {
      throw new Error(err)
    },
    variables: { id: postId, title, content },
  })

  const handleChange = (value) => {
    setContent(value)
  }

  if (error) return <Error statusCode={401} title={'Data fetching failed'} />
  if (loading) return <div>loading...</div>
  if (!loading && data) {
    return (
      <div>
        <Input
          fluid
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          style={{ marginBottom: 20 }}
        />
        <div style={{ marginBottom: 20 }}>
          <Editor handleChange={handleChange} content={content} />
        </div>

        <Button primary fluid onClick={updatePost}>
          수정
        </Button>
      </div>
    )
  }
}

export default Update
