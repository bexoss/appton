import { useState, useEffect, useContext } from 'react'
import { useRouter } from 'next/router'
import { useQuery, useLazyQuery, useMutation } from '@apollo/react-hooks'
import { AuthContext } from '../../context/auth'
import { GET_SINGLE_POST, REMOVE_POST, ADD_COMMENT, REMOVE_COMMENT } from '../../util/gql'
import { Button, Form, Comment, Header, Icon, Dimmer, Loader, Container } from 'semantic-ui-react'
import Link from 'next/link'
import styled from 'styled-components'
import Cookies from 'universal-cookie'
import Error from 'next/error'
import ClientOnly from '../ClientOnly'
import List from './List'
import moment from 'moment'

function DeleteButton({ commentId, postId, onSuccess, styled = true, size = 'tiny' }) {
  const [removeComment, { loading, error }] = useMutation(REMOVE_COMMENT, {
    update() {
      onSuccess && onSuccess()
    },
    variables: { commentId, postId },
  })

  const [removePost, { loading: loadingP, error: errorP }] = useMutation(REMOVE_POST, {
    update() {
      onSuccess && onSuccess()
    },
    variables: { id: postId },
  })

  return styled ? (
    <Button
      basic
      size={size}
      color='orange'
      onClick={() => {
        if (commentId) {
          const confirm = window.confirm('댓글을 삭제할까요?')
          if (confirm) removeComment()
        } else {
          const confirm = window.confirm('문서를 삭제할까요?')
          if (confirm) removePost()
        }
        return
      }}
    >
      삭제
    </Button>
  ) : (
    <span
      onClick={() => {
        if (commentId) {
          const confirm = window.confirm('댓글을 삭제할까요?')
          if (confirm) removeComment()
        } else {
          const confirm = window.confirm('문서를 삭제할까요?')
          if (confirm) removePost()
        }
        return
      }}
    >
      삭제
    </span>
  )
}

const PostContainer = styled.div`
  border-bottom: 1px solid #eee;
  margin-bottom: 20px;
`
const CommentsContainer = styled.div`
  padding: 20px 0;
`

const PostTitle = styled.div`
  font-size: 2rem;
  font-weight: 600;
  display: block;
  margin-bottom: 40px;
`

const PostHeader = styled.div`
  border-top: 2px solid #9b9fa8;
  padding: 20px 15px;
  border-bottom: 1px solid #eee;
`

const PostHeaderTitle = styled.div`
  font-size: 1.5rem;
  font-weight: 600;
  color: #333;
  margin: 3px 0 10px 0;
`

const PostHeaderSubtitle = styled.div`
  margin-top: 15px;
  color: #999;
  font-size: 1rem;
`

const PostContent = styled.div`
  padding: 20px;
  min-height: 200px;
`

const PostActionsContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  padding: 10px 0;
`

const Separator = () => <span style={{ marginLeft: 4, marginRight: 4, color: '#ddd' }}>|</span>

function Read(props) {
  const { postId, baseUrl, allowComment } = props
  const router = useRouter()
  const [comment, setComment] = useState('')
  const [showDeleteConfirmModal, setShowDeleteConfirmModal] = useState(false)
  const cookies = new Cookies()

  const [loadPost, { loading, data, error }] = useLazyQuery(GET_SINGLE_POST, {
    variables: { id: postId, dayFirstUnix: cookies.get('DAY_FIRST_UNIX') },
    fetchPolicy: 'cache-and-network',
  })

  // const { loading, data, error } = useQuery(GET_SINGLE_POST, {
  //   variables: { id: postId, dayFirstUnix: cookies.get('DAY_FIRST_UNIX') },
  //   fetchPolicy: 'cache-and-network',
  // })

  const { user } = useContext(AuthContext)

  const [addComment, { loadingR, errorR }] = useMutation(ADD_COMMENT, {
    update() {
      setComment('')
    },
    onError(err) {
      console.log(err)
    },
    variables: { postId: router.query.postId, content: comment },
  })

  const [removePost, { loadingRemovePost, errorRemovePost }] = useMutation(REMOVE_POST, {
    update() {
      setShowDeleteConfirmModal(false)
      router.push({
        pathname: router.pathname.split('/[')[0],
      })
    },
    onError(err) {
      throw new Error(err)
    },
    variables: { id: router.query.postId },
  })

  useEffect(() => {
    loadPost()
  }, [])

  if (error)
    return (
      <div>
        <Container>
          <div style={{ background: '#f1f1f1', padding: 10 }}>
            <h1>Unexpected Error</h1>
            <pre>{JSON.stringify(error, null, 4)}</pre>
          </div>
        </Container>
      </div>
    )

  if (!data)
    return (
      <div>
        <Container
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <div>
            <Loader active>Loading...</Loader>
          </div>
        </Container>
      </div>
    )

  return (
    <div>
      {!loading && !error && data.getSinglePost ? (
        <div>
          <PostContainer>
            {/* <PostTitle>{title}</PostTitle> */}
            <PostHeader>
              <PostHeaderTitle>{data.getSinglePost.title}</PostHeaderTitle>
              <PostHeaderSubtitle>
                작성자 {data.getSinglePost.user.name}
                <Separator />
                등록 {moment.unix(data.getSinglePost.createdAt).format('YYYY-MM-DD HH:mm')}
                <Separator />
                조회수 {data.getSinglePost.viewCount}
              </PostHeaderSubtitle>
            </PostHeader>
            <PostContent>
              <div className='ql-editor-content' dangerouslySetInnerHTML={{ __html: data.getSinglePost.content }} />
            </PostContent>

            <ClientOnly>
              <PostActionsContainer>
                <Link href={baseUrl}>
                  <Button primary size='tiny'>
                    목록
                  </Button>
                </Link>

                {user && user.email === data.getSinglePost.user.email && (
                  <>
                    <Link href={baseUrl + '?postId=' + postId + '&mode=update'}>
                      <Button basic size='tiny' primary>
                        수정
                      </Button>
                    </Link>
                    <DeleteButton size='tiny' postId={data.getSinglePost.id} onSuccess={() => router.push(baseUrl)} />
                  </>
                )}
              </PostActionsContainer>
            </ClientOnly>
          </PostContainer>

          {allowComment && (
            <CommentsContainer>
              <Header as='h3'>댓글 ({data.getSinglePost.comments.length})</Header>
              {data.getSinglePost.comments.length > 0 ? (
                data.getSinglePost.comments.map((comment) => {
                  return (
                    <Comment.Group key={comment._id}>
                      <Comment>
                        <Comment.Avatar src='/avatar.png' />
                        <Comment.Content>
                          <Comment.Author as='a'>{comment.user.name}</Comment.Author>

                          <Comment.Metadata>
                            <div>{moment.unix(comment.createdAt).fromNow()}</div>
                          </Comment.Metadata>
                          <Comment.Text>{comment.content}</Comment.Text>

                          <Comment.Actions>
                            {/* <Comment.Action>Reply</Comment.Action> */}
                            {user && user.email === comment.user.email && (
                              <Comment.Action>
                                <DeleteButton size='mini' postId={data.getSinglePost.id} commentId={comment._id} styled={false} />
                              </Comment.Action>
                            )}
                          </Comment.Actions>
                        </Comment.Content>
                      </Comment>
                    </Comment.Group>
                  )
                })
              ) : (
                <h4>댓글이 없습니다.</h4>
              )}

              <Form reply style={{ marginTop: 50 }}>
                <Form.TextArea value={comment} onChange={(e, d) => setComment(e.target.value)} disabled={!user} placeholder={user ? '' : '로그인 후 댓글을 작성할 수 있습니다.'} />
                <Button size='tiny' content='댓글 작성' primary onClick={addComment} disabled={!user} />
              </Form>
            </CommentsContainer>
          )}
        </div>
      ) : (
        <div>{error ? <Error statusCode={403} title={error.graphQLErrors[0] ? error.graphQLErrors[0].message : '오류가 발생했습니다. 다시 시도해 주세요.'} /> : 'loading...'}</div>
      )}

      <List {...props} current={postId} />
    </div>
  )
}

// export default Read
export default Read
