import React, { useState, useEffect, useContext } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { AuthContext } from '../../context/auth'
import { Container, Segment, Grid } from 'semantic-ui-react'
import { Site, Header, LogoWrapper, HeaderRow, Main, Footer, ResponsiveBR, ResponsiveSeparator } from '../Styles/Layout'

function Layout({ mobile, children }) {
  const router = useRouter()
  const context = useContext(AuthContext)

  useEffect(() => {
    if (context && router) {
      if (context.user) {
        if (router.pathname === '/login' || router.pathname === '/register') {
          router.push('/')
        }
      } else {
        if (router.pathname === '/profile') {
          router.push('/')
        }
      }
    }
  }, [])

  return (
    <Site>
      <Header isIndex={router.pathname === '/'}>
        <Container>
          <HeaderRow>
            <LogoWrapper>
              <span className='kanit' style={{ color: '#222' }}>
                Appton Inc.
              </span>
            </LogoWrapper>
          </HeaderRow>
        </Container>
      </Header>

      <Main>
        <div>{children}</div>
      </Main>

      <Footer className='sans'>
        <Segment
          vertical
          style={{
            padding: '1em 0em',
            borderTop: '1px solid #eee',
            fontSize: '0.9rem',
          }}
        >
          <Container text>
            <Grid divided stackable>
              <Grid.Row>
                <Grid.Column width={16}>
                  <div style={{ color: '#aaa', lineHeight: '1.8rem', fontSize: '1rem' }} className='sans'>
                    (주)앱톤 <ResponsiveSeparator>|</ResponsiveSeparator> 서울시 종로구 종로 51 종로타워 18층
                    <br />
                    사업자등록번호 897-87-01516 <ResponsiveSeparator>|</ResponsiveSeparator>대표자 김준태 <ResponsiveSeparator>|</ResponsiveSeparator>
                    <ResponsiveBR /> 전화 1544-3197 | 이메일 cs@appton.co.kr
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Container>
        </Segment>
      </Footer>
    </Site>
  )
}

export default Layout
