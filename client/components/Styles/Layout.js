import styled, { css } from 'styled-components'
import { Container, Segment } from 'semantic-ui-react'

export const headerHeight = 88
export const mobileHeaderHeight = 80
export const headerBorderBottomColor = '#EFEFF4'
export const mainBackgroundColor = '#fff'
export const headerBackgroundColor = '#F7F7F7'

export const Site = styled.div`
`
export const Header = styled.div`
  width: 100%;
  height: 70px;
  display: flex;
  flex-direction: row;
  align-items: center;
  background: #f4f5f7;
  @media (max-width: 767px) {
    color: #000;
  }
`

export const Main = styled.main`
`

export const HeaderRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`


export const LogoWrapper = styled.div`
  width: 116px;
  font-size: 20px;
  color: #000;
  font-family: 'Lexend Tera', sans-serif;
  font-weight: 600;
  font-size: 1.6rem;
  @media (max-width: 767px) {
    margin-left: 5px;
  }
`



export const Footer = styled.div`
  
`

export const SideMenuWrapper = styled.div`
  height: 48px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background: #fff;
  border-bottom: 1px solid #eee;
`

export const SideBarWrapper = styled.div``

export const PaperTitle = styled.h2`
  font-size: 1.8rem;
  letter-spacing: -1px;
  margin: 0;
`

export const ResponsiveBR = styled.div`
  display: inline-block;
  @media (max-width: 767px) {
    display: block;
  }
`

export const ResponsiveSeparator = styled.span`
  margin: 0px 4px;
  color: #ccc;
  ${(props) =>
    props.hideMobile &&
    css`
      @media (max-width: 767px) {
        display: none;
      }
    `}
`

export const ResponsiveContainer = styled(Container)`
  @media (max-width: 767px) {
    margin: 0 !important;
    margin-top: 10px !important;
    padding: 0 !important;
  }
`

export const ResponsiveSegment = styled(Segment)`
  @media (max-width: 767px) {
    padding: 10px !important;
    /* border: none !important; */
    /* -webkit-box-shadow: none !important;
    -moz-box-shadow: none !important;
    box-shadow: none !important; */
  }
`
export const HorizontalSpaceBetween = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

export const SmallPaper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  max-width: 450px;
  background: #fff;
  padding: 30px;
  margin: 0 auto;
  margin-top: 120px;
  min-height: 300px;
  box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15);

  @media (max-width: 767px) {
    margin-top: 30px;
    -webkit-box-shadow: none !important;
    -moz-box-shadow: none !important;
    box-shadow: none !important;
  }
`

export const DesktopOnly = styled.div`
  @media (max-width: 767px) {
    display: none;
  }
`

export const MobileOnly = styled.div`
  display: none;
  @media (max-width: 767px) {
    display: flex;
  }
`

export const SideMenu = styled.div`
  text-decoration: none;
  padding: 15px;
  font-size: 1rem;
  cursor: pointer;

  ${(props) =>
    props.active &&
    css`
      font-weight: 600;
    `}
`
