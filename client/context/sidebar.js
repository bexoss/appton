import React, { useReducer, createContext } from 'react'
import Router from 'next/router'
import { initial } from 'lodash'
import { useWindowDimensions } from '../util/hooks'

const initialState = {
  visible: true,
}

const SidebarContext = createContext({
  visible: true,
  hide: () => { },
  show: () => { }
})


function reducer(state, action) {
  switch (action.type) {
    case 'HIDE':
      return {
        ...state,
        visible: false,
      }
    case 'SHOW':
      return {
        ...state,
        visible: true,
      }
    default:
      return state
  }
}

function SidebarProvider(props) {
  const [state, dispatch] = useReducer(reducer, initialState)

  function hide() {
    dispatch({
      type: 'HIDE',
    })
  }

  function show() {
    dispatch({
      type: 'SHOW'
    })
  }

  return <SidebarContext.Provider value={{ visible: state.visible, hide, show }} {...props} />
}


export { SidebarContext, SidebarProvider }
