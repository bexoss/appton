import React, { useReducer, createContext } from 'react'
import Router from 'next/router'

const initialState = {
  product: null,
}

const ProductContext = createContext({
  product: null,
  mountProduct: (product) => {
    console.log('mount product... on createContext')
    console.log(product)
  },
  unmountProduct: () => {},
})

function reducer(state, action) {
  switch (action.type) {
    case 'MOUNT_PRODUCT':
      return {
        ...state,
        product: action.payload,
      }
    case 'UNMOUNT_PRODUCT':
      return {
        ...state,
        product: null,
      }
    default:
      return state
  }
}

function ProductProvider(props) {
  const [state, dispatch] = useReducer(reducer, initialState)
  
  function mountProduct(product) {
    console.log('mount product...')
    console.log(product)

    dispatch({
      type: 'MOUNT_PRODUCT',
      payload: product,
    })
    Router.push(`/cosmetics/${product.title.replace(/ /g, '-')}`)

  }

  function unmountProduct() {
    dispatch({ type: 'UNMOUNT_PRODUCT' })
  }

  return <ProductContext.Provider value={{ product: state.product, mountProduct, unmountProduct }} {...props} />
}


export { ProductContext, ProductProvider }
