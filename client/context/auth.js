import React, { useReducer, createContext } from 'react'
import jwtDecode from 'jwt-decode'
import Cookies from 'universal-cookie'
import moment from 'moment'

const initialState = {
  user: null,
}

const AuthContext = createContext({
  user: null,
  login: (userData) => {},
  logout: () => {},
})

function authReducer(state, action) {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        user: action.payload,
      }
    case 'LOGOUT':
      return {
        ...state,
        user: null,
      }
    default:
      return state
  }
}

function AuthProvider(props) {
  const [state, dispatch] = useReducer(authReducer, initialState)
  const cookies = new Cookies()
  const token = cookies.get('jwtToken')

  if (token) {
    const decodedToken = jwtDecode(token)
    if (decodedToken.exp * 1000 < Date.now()) {
      cookies.remove('jwtToken', { path: "/" })
    } else {
      initialState.user = decodedToken
    }
  }
  
  function login(userData) {

    console.log('login...')
    console.log(userData)
    if (userData.token) {
      cookies.set("jwtToken", userData.token, { path: "/", expires: moment().add(365, 'days').toDate() })
    }
    dispatch({
      type: 'LOGIN',
      payload: userData,
    })
  }

  function logout() {
    cookies.remove('jwtToken', { path: "/" })
    dispatch({ type: 'LOGOUT' })
    
  }
  return <AuthContext.Provider value={{ user: state.user, login, logout }} {...props} />
}


export { AuthContext, AuthProvider }
