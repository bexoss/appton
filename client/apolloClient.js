import { ApolloClient } from 'apollo-client'
import {
    InMemoryCache,
} from 'apollo-cache-inmemory'
import { createHttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import fetch from 'isomorphic-unfetch'
import getConfig from 'next/config'

const { publicRuntimeConfig } = getConfig()
const { NODE_ENV } = publicRuntimeConfig


const authLink = setContext((_, { headers }) => {

    // get the authentication token from cookie if it exists
    let token
    if (typeof window !== 'undefined') {
        if (document.cookie.includes('jwtToken')) {
            token = document.cookie.split('jwtToken=')[1].split(';')[0]
        }
    }

    // return the headers to the context so httpLink can read them
    return {
        headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : '',
            // 'x-real-ip': headers['X-Real-IP'] ? headers['X-Real-IP'] : ''
        },
    }
})

export default function createApolloClient(initialState, ctx) {
    // The `ctx` (NextPageContext) will only be present on the server.
    // use it to extract auth headers (ctx.req) or similar.

    // https://www.apollographql.com/docs/react/networking/authentication/

    return new ApolloClient({
        ssrMode: Boolean(ctx),
        link: authLink.concat(
            createHttpLink({
                uri:
                    NODE_ENV === 'development'
                        ? 'http://localhost:4000/graphql'
                        : 'https://appton.co.kr/graphql',
                credentials: 'same-origin',
                fetch,
            })
        ),
        cache: new InMemoryCache(initialState),
        credentials: 'include',
    })
}
